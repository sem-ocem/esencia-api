package co.edu.medellin.sem.ocem.esencia.util.date;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.TextStyle;
import java.util.Date;
import java.util.Locale;

/**
 * Esta clase se encarga de formatear las fechas o de realizar un convert a los datos que manejan las fechas
 *
 * @author israel.villegas@medellin.gov.co
 */
public class DateUtil {

    /**
     * Este metodo obtiene el mes anterior al actual como String, donde retorna el nombre del mes
     * @return String
     */
    static public String getMonth (){
        Month month = LocalDate.now().getMonth();
        String monthName = month.getDisplayName(TextStyle.FULL, new Locale("es", "COL"));
        return monthName;
    }

    /**
     * Este metodo obtiene el mes anterior al actual como int, donde retorna el numero del del mes
     * @return
     */
    static public int getMonthNumber (){
        Month month = LocalDate.now().getMonth();//.plus(2l);//.minus(1L);
        int monthNumber = month.getValue();
        return monthNumber;
    }

    /**
     * Este metodo obtiene el en anio presente
     * @return
     */
    static public int getYear (){
        return LocalDate.now().getYear();
    }

    /**
     * Este metodo es el encargado de comparar los dos parametros de entrada que son datos tipo Date
     * actual
     * @param date
     * @param date2
     * @return int
     */
    public static int compareTo(Date date, Date date2) {
        if (date2 == null)
            return 1;
        return date.compareTo(date2);
    }
}
