package co.edu.medellin.sem.ocem.esencia.util.exception;

import lombok.Data;

@Data
public class CustomException extends Exception {

    private String variableFile;

    public CustomException(String msg, String variableFile) {
        super(msg);
        this.variableFile = variableFile;
    }

    public CustomException(String msg, Exception e) {
        super(msg, e);
    }

}
