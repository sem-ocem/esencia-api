package co.edu.medellin.sem.ocem.esencia.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Esta clase es donde donde se en cuentra la valores constantes de la aplicacion
 *
 * @author israel.villegas@medellin.gov.co
 */
public class Constant {

    public static final String[] PROPIEDADES_ANEXO = {
            "anio", "numCod", "daneCodigo", "daneAnterior", "sedeConsecutivo", "documentoCod", "numDocumento",
            "expDepartamentoCod", "expMunicipioCod", "apellido1", "apellido2", "nombre1", "nombre2",
            "direccionResidencia", "telefono", "resDepartamentoCod", "resMunicipioCod", "estrato", "sisben",
            "fechaNacimiento", "nacDepartamentoCod", "nacMunicipioCod", "genero", "pobVictConfCod",
            "expulDepartamentoCod", "expulMunicipioCod", "provieneSectorPriv", "provieneOtrMun",
            "discapacidadCod", "capacidadCod", "etniaCod", "resguardoCod", "insFamiliar", "jornadaCod",
            "caracterCod", "especialidadCod", "gradoCod", "grupo", "metodologiaCod", "matriculaContratada",
            "repitente", "nuevo", "sitAcadAnioAnteriorCod", "condAcadAnioAnteriorCod", "fuenteRecursosCod",
            "zonaCod", "madreCabezaFamilia", "hijoMadreCabezaFamilia", "veterano", "heroe", "internadoCod",
            "codigoValoracion1", "codigoValoracion2", "numConvenio", "personaCodigo", "apoyoAcademicoEspecialCod",
            "sistemaRespPenalCod", "paisOrigenCod", "trastornosEspecificosCod"
    };

    public static final Long CODIGO_NO_APLICA_TIPO_VICTIMA_CONFLICTO = 99L;
    public static final Long CODIGO_NO_APLICA_APOYO_ACADEMICO_ESPECIAL = 1L;
    public static final Long CODIGO_NO_APLICA_TIPO_CAPACIDAD = 9L;
    public static final Long CODIGO_NO_APLICA_ZONA = 3L;
    public static final Long CODIGO_NO_APLICA_SISTEMA_RESP_PENAL = 1L;
    public static final Long CODIGO_NO_APLICA_TRASTORNOS_ESPECIFICOS = 9L;
    public static final Long CODIGO_NO_APLICA_COND_ACAD_ANIOS_ANTERIORES = 9L;
    public static final Long CODIGO_NO_APLICA_ESPECIALIDADES= 0L;
    public static final Long CODIGO_NO_APLICA_CARACTER= 0L;
    public static final String ANEXO_5A = "5a";
    public static final String ANEXO_5B = "5b";
    public static final String ANEXO_5O = "5o";
    public static final String ANEXO_6A = "6a";
    public static final String ANEXO_6O = "6o";
    public static final String SECTOR_OFICIAL = "OFICIAL";
    public static final String SECTOR_COBERTURA_CONTRATADA = "COBERTURA CONTRATADA";
    public static final String SECTOR_PRIVADO = "PRIVADO";
    public static final String ESTUDIANTE = "ESTUDIANTE";
    public static final String TALENTO_HUMANO = "TALENTO_HUMANO";
    public static final String NO_SUPPORT_THESE_TYPE = "Cannot handle cells of type 5";


}
