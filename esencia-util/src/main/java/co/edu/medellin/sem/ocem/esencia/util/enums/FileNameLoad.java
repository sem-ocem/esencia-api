package co.edu.medellin.sem.ocem.esencia.util.enums;

import co.edu.medellin.sem.ocem.esencia.util.date.DateUtil;

public enum FileNameLoad {
    ANEXO_5A(String.valueOf(DateUtil.getYear()).concat(String.valueOf(DateUtil.getMonthNumber())).concat(".txt")),
    ANEXO_5B(String.valueOf(DateUtil.getYear()).concat(String.valueOf(DateUtil.getMonthNumber())).concat(".txt")),
    ANEXO_5O(String.valueOf(DateUtil.getYear()).concat(String.valueOf(DateUtil.getMonthNumber())).concat(".txt")),
    ANEXO_6A(String.valueOf(DateUtil.getYear()).concat(String.valueOf(DateUtil.getMonthNumber())).concat(".txt")),
    ANEXO_6O(String.valueOf(DateUtil.getYear()).concat(String.valueOf(DateUtil.getMonthNumber())).concat(".txt")),
    DUE(String.valueOf(DateUtil.getYear()).concat(String.valueOf(DateUtil.getMonthNumber())).concat(".xlsx"));

    private String nameComplementFile;

    private FileNameLoad(String nameComplementFile){
        this.nameComplementFile = nameComplementFile;
    }

    public String getNameComplementFile(){
        return this.nameComplementFile;
    }

}
