package co.edu.medellin.sem.ocem.esencia.repository.port;

import co.edu.medellin.sem.ocem.esencia.repository.entity.EstudianteEspecialidad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EstudianteEspecialidadRepository extends JpaRepository<EstudianteEspecialidad, Long> {

    @Query("select e from EstudianteEspecialidad e where e.personaCod = :personaCod order by e.codigo desc")
    List<EstudianteEspecialidad> findByPersonaCod(@Param("personaCod") Long personaCod);
}
