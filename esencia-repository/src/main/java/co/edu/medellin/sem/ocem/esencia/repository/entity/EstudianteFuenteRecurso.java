package co.edu.medellin.sem.ocem.esencia.repository.entity;

import co.edu.medellin.sem.ocem.esencia.repository.entity.support.Auditable2;
import lombok.Data;

import javax.persistence.*;

/**
 * Esta clase es para obtener las fuentes de recursos  que puede tener los estudiantes y talento humano
 *
 * @author israel.villegas@medellin.gov.co
 */

@Entity
@Data
@Table(name = "eo_tbl_estudiantes_fuentes_recursos")
public class EstudianteFuenteRecurso extends Auditable2 {

    /**
     * Atributo que obtiene o almacena el codigo de la tabla
     */
    @Id
    @Column(name = "codigo", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "eo_seq_estudiantes_fuentes_recursos")
    @SequenceGenerator(name = "eo_seq_estudiantes_fuentes_recursos", sequenceName = "eo_seq_estudiantes_fuentes_recursos", allocationSize = 1)
    private Long codigo;

    /**
     * Atributo que obtiene o almacena el codigo que identifica la persona (Estudiante) en el sistema
     */
    @Column(name = "persona_cod", nullable = false)
    private Long personaCod;

    /**
     * Atributo que obtiene o almacena el codigo de la fuente de recurso que viene del archivo plano SIMAT
     */
    @Column(name = "fuente_recurso_cod", nullable = false)
    private Long fuenteRecursosCod;



}
