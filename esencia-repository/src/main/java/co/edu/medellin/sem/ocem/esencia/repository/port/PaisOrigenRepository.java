package co.edu.medellin.sem.ocem.esencia.repository.port;

import co.edu.medellin.sem.ocem.esencia.repository.entity.PaisOrigen;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PaisOrigenRepository extends JpaRepository<PaisOrigen, Long> {

    Optional<PaisOrigen> findByCodigo(Long codigo);
}
