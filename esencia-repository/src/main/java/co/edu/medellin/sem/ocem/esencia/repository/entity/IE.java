package co.edu.medellin.sem.ocem.esencia.repository.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * Esta clase es para obtener la entidad IE que son las instituciones de Medellin
 *
 * @author israel.villegas@medellin.gov.co
 */
@Entity
@Data
@Table(name = "eo_tbl_ies")
public class IE {

    /**
     * Atributo que obtiene o almacena el codigo Dane que identifica la Institucion
     */
    @Id
    @Column(name = "dane_anterior", nullable = false)
    private Long daneAnterior;

    /**
     * Atributo que obtiene o almacena el codigo de la division politica administrativa
     */
    @Column(name = "zona_division_politica_admin_cod", nullable = false)
    private Long zonaDivisionPoliticaAdminCod;

    /**
     * Atributo que obtiene o almacena el codigo del nit
     */
    @Column(name = "nit")
    private String nit;

    /**
     * Atributo que obtiene o almacena el codigo Dane de establecimiento que pertenece la IE; unas IEs son identificadas
     * como establecimiento.
     */
    @Column(name = "dane_establecimiento", nullable = false)
    private Long daneEstablecimiento;

    /**
     * Atributo que obtiene o almacena el codigo el consecutivo Dane
     */
    @Column(name = "dane_consecutivo", nullable = false)
    private Long daneConsecutivo;


    @Column(name = "dane_anterior_sector")
    private String daneAnteriorSector;

    /**
     * Atributo que obtiene o almacena el nombre de la sede
     */
    @Column(name = "nombre_sede")
    private String nombreSede;

    /**
     * Atributo que obtiene o almacena el nombre de tipo de sede
     */
    @Column(name = "tipo_sede")
    private String tipoSede;

    /**
     * Atributo que obtiene o almacena el codigo de la comuna
     */
    @Column(name = "comuna_Cod", nullable = false)
    private Long comunaCod;


    /**
     * Atributo que obtiene o almacena el codigo del nucleo
     */
    @Column(name = "nucleo", nullable = false)
    private Integer nucleo;

    /**
     * Atributo que obtiene o almacena el numero oficial
     */
    @Column(name = "numero_oficial", nullable = false)
    private Integer numeroOficial;

    /**
     * Atributo que obtiene o almacena el nombre de servicio que ofrece la IE
     */
    @Column(name = "prestacion_servicio")
    private String prestacionServicio;

    /**
     * Atributo que obtiene o almacena el sector de la IE
     */
    @Column(name = "sector")
    private String sector;

    /**
     * Atributo que obtiene o almacena la zona sineb
     */
    @Column(name = "zona_sineb")
    private String zonaSineb;

    /**
     * Atributo que obtiene o almacena la zona georeferenciado
     */
    @Column(name = "zona_georeferenciado")
    private String zonaGeoreferenciado;

    /**
     * Atributo que obtiene o almacena el Barrio
     */
    @Column(name = "barrio")
    private String barrio;

    /**
     * Atributo que obtiene o almacena la vereda
     */
    @Column(name = "vereda")
    private String vereda;

    /**
     * Atributo que obtiene o almacena la direccion de la IE
     */
    @Column(name = "direccion")
    private String direccion;

    /**
     * Atributo que obtiene o almacena el telefono de la IE
     */
    @Column(name = "telefono")
    private String telefono;

    /**
     * Atributo que obtiene o almacena el correo de la IE
     */
    @Column(name = "correo_electronico", length = 600)
    private String correoElectronico;

    /**
     * Atributo que obtiene o almacena el correo de la IE
     */
    @Column(name = "rector")
    private String rector;

    /**
     * Atributo que obtiene o almacena el numero de cedula del rector
     */
    @Column(name = "rector_Cedula")
    private Long rectorCedula;

    /**
     * Atributo que obtiene o almacena las observaciones de la IE
     */
    @Column(name = "observaciones", length = 600)
    private String observaciones;

    /**
     * Atributo que obtiene o almacena las observaciones de la IE
     */
    @Column(name = "celulares", length = 600)
    private String celulares;

    /**
     * Atributo donde referencia que una IE una o muchas consistencia
     */
    @Transient
    private ConsistenciaIE consistenciaIE;
}
