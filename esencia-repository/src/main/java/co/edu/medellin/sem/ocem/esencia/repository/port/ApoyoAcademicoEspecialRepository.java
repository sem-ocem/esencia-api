package co.edu.medellin.sem.ocem.esencia.repository.port;

import co.edu.medellin.sem.ocem.esencia.repository.entity.ApoyoAcademicoEspecial;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ApoyoAcademicoEspecialRepository extends JpaRepository<ApoyoAcademicoEspecial, Long> {

    Optional<ApoyoAcademicoEspecial> findByCodigo(Long codigo);
}
