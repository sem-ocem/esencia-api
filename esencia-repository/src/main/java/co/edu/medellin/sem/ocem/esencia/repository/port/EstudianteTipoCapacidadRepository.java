package co.edu.medellin.sem.ocem.esencia.repository.port;

import co.edu.medellin.sem.ocem.esencia.repository.entity.EstudianteTipoCapacidad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EstudianteTipoCapacidadRepository extends JpaRepository<EstudianteTipoCapacidad, Long> {

    @Query("select e from EstudianteTipoCapacidad e where e.personaCod = :personaCod order by e.codigo desc")
    List<EstudianteTipoCapacidad> findByPersonaCod(@Param("personaCod") Long personaCod);
}
