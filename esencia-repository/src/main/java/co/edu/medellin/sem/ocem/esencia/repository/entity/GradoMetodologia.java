package co.edu.medellin.sem.ocem.esencia.repository.entity;

import co.edu.medellin.sem.ocem.esencia.repository.entity.support.Auditable2;
import lombok.Data;

import javax.persistence.*;

/**
 * Esta clase es para obtener los las metodologias de los estudiantes por medio del objeto grado
 *
 * @author israel.villegas@medellin.gov.co
 */

@Entity
@Data
@Table(name = "eo_tbl_grados_metodologias")
public class GradoMetodologia extends Auditable2 {

    /**
     * Atributo que obtiene o almacena el codigo de la tabla
     */
    @Id
    @Column(name = "codigo", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "eo_seq_grados_metodologias")
    @SequenceGenerator(name = "eo_seq_grados_metodologias", sequenceName = "eo_seq_grados_metodologias", allocationSize = 1)
    private Long codigo;

    /**
     * Atributo que obtiene o almacena el codigo que identifica la metodologia en el sistema
     */
    @Column(name = "metodologia_cod", nullable = false)
    private Long metodologiaCod;

    /**
     * Atributo que obtiene o almacena el codigo que identifica el grado
     */
    @Column(name = "grado_cod", nullable = false)
    private Long gradoCod;

    /**
     * Atributo que obtiene o almacena el anio del registro y se identifica como anio y por base de datos se identifica
     * como anio
     */
    @Column(name = "anio_matricula", nullable = false)
    private Long anioMatricula;

    /**
     * Atributo que obtiene o almacena el mes del anio en formato numero en que es el registro del SIMAT
     */
    @Column(name = "mes_matricula", nullable = false, length = 2)
    private int MesMatricula;
}
