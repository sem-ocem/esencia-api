package co.edu.medellin.sem.ocem.esencia.repository.port;

import co.edu.medellin.sem.ocem.esencia.repository.entity.FuenteRecurso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FuenteRecursoRepository extends JpaRepository<FuenteRecurso, Long> {

    Optional<FuenteRecurso> findByfuenteRecursosEstandarCodAndPara(Long fuenteRecursosEstandarCod, String para);
}
