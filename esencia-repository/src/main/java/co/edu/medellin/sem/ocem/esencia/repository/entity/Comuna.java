package co.edu.medellin.sem.ocem.esencia.repository.entity;


import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Esta clase es para obtener la entidad comuna
 *
 * @author israel.villegas@medellin.gov.co
 */
@Entity
@Data
@Table(name = "eo_tbl_comunas")
public class Comuna {

    /**
     * Atributo que obtiene o almacena el codigo que identifica la comuna
     */
    @Id
    @Column(name = "codigo", nullable = false)
    private Long codigo;

    /**
     * Atributo que obtiene o almacena el nombre de la comuna
     */
    @Column(name = "nombre", length = 100)
    private String nombre;

    /**
     * Atributo que obtiene o almacena la descripcion de la comuna
     */
    @Column(name = "descripcion", length = 1000)
    private String descripcion;
}
