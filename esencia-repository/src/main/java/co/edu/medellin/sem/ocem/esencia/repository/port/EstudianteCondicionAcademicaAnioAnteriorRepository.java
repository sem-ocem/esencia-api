package co.edu.medellin.sem.ocem.esencia.repository.port;

import co.edu.medellin.sem.ocem.esencia.repository.entity.EstudianteCondicionAcademicaAnioAnterior;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EstudianteCondicionAcademicaAnioAnteriorRepository extends JpaRepository<EstudianteCondicionAcademicaAnioAnterior, Long> {

    @Query("select e from EstudianteCondicionAcademicaAnioAnterior e where e.personaCod = :personaCod order by e.codigo desc")
    List<EstudianteCondicionAcademicaAnioAnterior> findByPersonaCod(@Param("personaCod") Long personaCod);

}
