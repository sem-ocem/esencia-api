package co.edu.medellin.sem.ocem.esencia.repository.port;

import co.edu.medellin.sem.ocem.esencia.repository.entity.EstudianteExpedicionDocumento;
import co.edu.medellin.sem.ocem.esencia.repository.entity.EstudianteResidencia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EstudianteExpedicionDocumentoRepository extends JpaRepository<EstudianteExpedicionDocumento, Long> {

    EstudianteExpedicionDocumento findByPersonaCodAndMunicipioCod(Long personaCod, Long municipioCod);
}
