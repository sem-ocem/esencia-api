package co.edu.medellin.sem.ocem.esencia.repository.port;

import co.edu.medellin.sem.ocem.esencia.repository.entity.EstudianteGrado;
import co.edu.medellin.sem.ocem.esencia.repository.entity.GradoMetodologia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GradoMetodologiaRepository extends JpaRepository<GradoMetodologia, Long> {

    @Query("select m from GradoMetodologia m, Estudiante e  where m.gradoCod = e.gradoCod and e.personaCodigo = :personaCod order by m.codigo desc")
    List<GradoMetodologia> findByPersonaCod(@Param("personaCod") Long personaCod);
}
