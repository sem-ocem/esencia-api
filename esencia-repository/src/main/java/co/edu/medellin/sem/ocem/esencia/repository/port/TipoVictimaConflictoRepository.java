package co.edu.medellin.sem.ocem.esencia.repository.port;

import co.edu.medellin.sem.ocem.esencia.repository.entity.TipoVictimaConflicto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface TipoVictimaConflictoRepository extends JpaRepository<TipoVictimaConflicto, Long> {

    Optional<TipoVictimaConflicto> findByCodigo(Long codigo);
}
