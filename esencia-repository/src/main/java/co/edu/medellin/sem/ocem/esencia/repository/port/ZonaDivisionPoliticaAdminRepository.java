package co.edu.medellin.sem.ocem.esencia.repository.port;

import co.edu.medellin.sem.ocem.esencia.repository.entity.Zona;
import co.edu.medellin.sem.ocem.esencia.repository.entity.ZonaDivisionPoliticaAdmin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ZonaDivisionPoliticaAdminRepository extends JpaRepository<ZonaDivisionPoliticaAdmin, Long> {

    Optional<ZonaDivisionPoliticaAdmin> findByCodigo(Long codigo);
}
