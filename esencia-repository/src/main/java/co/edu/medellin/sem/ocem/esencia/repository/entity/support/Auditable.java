package co.edu.medellin.sem.ocem.esencia.repository.entity.support;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import java.util.Date;

/**
 * Esta clase se encarga de tener la informacion para auditar los Entity que hereden de de esta clase
 *
 * @author israel.villegas@medellin.gov.co
 */
@Data
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class Auditable {

    /**
     * Variable global que se encarga de almacenar la fecha cuando hizo la accion de crear al objeto que herede de esta
     * clase y esta mapeada a la columna de la base de la datos
     */
    @CreatedDate
    @Column(name = "fe_creado")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date feCreado;


    /**
     * Variable global que se encarga de almacenar la fecha cuando hizo la accion de actualizar al objeto que herede de
     * esta clase
     */
    @LastModifiedDate
    @Column(name = "fe_modificado")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date feModificado;
}
