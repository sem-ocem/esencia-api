package co.edu.medellin.sem.ocem.esencia.repository.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * Esta clase es para obtener la entidad ConsistenciaIE donde alamacena la persistencia de las instituciones educativas
 *
 * @author israel.villegas@medellin.gov.co
 */
@Entity
@Data
@Table(name = "eo_tbl_consistencia_ie")
public class ConsistenciaIE {


    /**
     * Atributo que obtiene o almacena el codigo que identifica el la consistencia de la IE
     */
    @Id
    @Column(name = "codigo", nullable = false)
    private Long codigo;

    /**
     * Atributo que obtiene o almacena el anio del registro y se identifica como anio y por base de datos se identifica
     * como anio
     */
    @Column(name = "anio", nullable = false)
    private Long anio;

    /**
     * Atributo que obtiene o almacena el codigo que identifica la institucion en el sistema
     */
    @Column(name = "dane_anterior", nullable = false)
    private Long daneAnterior;

    /**
     * Atributo que obtiene o almacena el mes del anio en formato numero en que es el registro del DUE
     */
    @Column(name = "mes_Formato_Numero", nullable = false, length = 2)
    private int mesFormatoNumero;
}

