package co.edu.medellin.sem.ocem.esencia.repository.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Esta clase es para obtener la entidad de la condicion academica del anio anterior del estudiante
 *
 * @author israel.villegas@medellin.gov.co
 */
@Entity
@Data
@Table(name = "eo_tbl_cond_acad_anios_anteriores")
public class CondicionAcademicaAnioAnterior {

    /**
     * Atributo que obtiene o almacena el codigo que identifica la condicion academica de anio anterior
     */
    @Id
    @Column(name = "codigo", nullable = false)
    private Long codigo;

    /**
     * Atributo que obtiene o almacena el nombre de la condicion academica de anio anterior
     */
    @Column(name = "nombre", length = 100)
    private String nombre;

    /**
     * Atributo que obtiene o almacena la descripcion de la condicion academica de anio anterior
     */
    @Column(name = "descripcion", length = 100)
    private String descripcion;
}
