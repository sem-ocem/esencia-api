package co.edu.medellin.sem.ocem.esencia.repository.port;

import co.edu.medellin.sem.ocem.esencia.repository.entity.EstudianteFuenteRecurso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EstudianteFuenteRecursosRepository extends JpaRepository<EstudianteFuenteRecurso, Long> {

    @Query("select e from EstudianteFuenteRecurso e where e.personaCod = :personaCod and e.fuenteRecursosCod = :fuenteRecursosCod order by e.codigo desc")
    List<EstudianteFuenteRecurso> findByPersonaCodAndFuenteRecursoCod(@Param("personaCod") Long personaCod, @Param("fuenteRecursosCod") Long fuenteRecursosCod);

    @Query("select e from EstudianteFuenteRecurso e where e.personaCod = :personaCod order by e.codigo desc")
    List<EstudianteFuenteRecurso> findByPersonaCod(@Param("personaCod") Long personaCod);

}
