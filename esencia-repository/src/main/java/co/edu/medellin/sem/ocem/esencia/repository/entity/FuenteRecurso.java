package co.edu.medellin.sem.ocem.esencia.repository.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Esta clase es para obtener los tipos de fuente de recursos para estudiante y talento humano
 *
 * @author israel.villegas@medellin.gov.co
 */
@Entity
@Data
@Table(name = "eo_tbl_fuentes_recursos")
public class FuenteRecurso {

    /**
     * Atributo que obtiene o almacena el codigo que identifica el tipo de documento
     */
    @Id
    @Column(name = "codigo", nullable = false)
    private Long codigo;

    /**
     * Atributo que obtiene o almacena el codigo de la fuente de recursos que viene del archivo plano SIMAT
     */
    @Column(name = "fuente_recursos_estandar_cod", nullable = false)
    private Long fuenteRecursosEstandarCod;

    /**
     * Atributo que obtiene o almacena para quien es la fuente de recursos(Estudiante, Talento humano)
     */
    @Column(name = "para", length = 50)
    private String para;

    /**
     * Atributo que obtiene o almacena el nombre del tipo de documento
     */
    @Column(name = "nombre", length = 100)
    private String nombre;

    /**
     * Atributo que obtiene o almacena la descripcion del tipo de documento
     */
    @Column(name = "descripcion", length = 100)
    private String descripcion;
}
