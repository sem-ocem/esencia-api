package co.edu.medellin.sem.ocem.esencia.repository.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * Esta clase es para obtener la entidad municipio
 *
 * @author israel.villegas@medellin.gov.co
 */
@Entity
@Data
@Table(name = "eo_tbl_municipios")
public class Municipio {

    /**
     * Atributo que obtiene o almacena el codigo de la tabla
     */
    @Id
    @Column(name = "codigo", nullable = false)
    private Long codigo;

    /**
     * Atributo que obtiene o almacena el codigo del municipio que viene del archivo plano SIMAT
     */
    @Column(name = "municipio_estandar_cod", nullable = false)
    private Long municipioEstandarCod;

    /**
     * Atributo que obtiene o almacena el nombre del municipio
     */
    @Column(name = "nombre", length = 100)
    private String nombre;

    @Column(name = "departamento_estandar_cod")
    private Long departamentoEstandarCod;

}
