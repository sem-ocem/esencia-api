package co.edu.medellin.sem.ocem.esencia.repository.port;

import co.edu.medellin.sem.ocem.esencia.repository.entity.EstudianteRespPenal;
import co.edu.medellin.sem.ocem.esencia.repository.entity.EstudianteTrastornoEspecifico;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EstudianteTrastornoEspecificoRepository extends JpaRepository<EstudianteTrastornoEspecifico, Long> {

    @Query("select e from EstudianteTrastornoEspecifico e where e.personaCod = :personaCod order by e.codigo desc")
    List<EstudianteTrastornoEspecifico> findByPersonaCod(@Param("personaCod") Long personaCod);
}
