package co.edu.medellin.sem.ocem.esencia.repository.entity;


import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Esta clase es para obtener la entidad de tipo discapacidad
 *
 * @author israel.villegas@medellin.gov.co
 */
@Entity
@Data
@Table(name = "eo_tbl_tipos_discapacidad")
public class TipoDiscapacidad {

    /**
     * Atributo que obtiene o almacena el codigo que identifica la discapacidad
     */
    @Id
    @Column(name = "codigo", nullable = false)
    private Long codigo;

    /**
     * Atributo que obtiene o almacena el nombre del tipo de discapacidad
     */
    @Column(name = "nombre", length = 100)
    private String nombre;

    /**
     * Atributo que obtiene o almacena la descripcion del tipo del tipo de discapacidad
     */
    @Column(name = "descripcion", length = 100)
    private String descripcion;
}
