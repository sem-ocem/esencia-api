package co.edu.medellin.sem.ocem.esencia.repository.entity;


import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Esta clase es para obtener la entidad de apoyo academico especiales
 *
 * @author israel.villegas@medellin.gov.co
 */
@Entity
@Data
@Table(name = "eo_tbl_apoyos_acads_especiales")
public class ApoyoAcademicoEspecial {

    /**
     * Atributo que obtiene o almacena el codigo que identifica el apoyo academico especial
     */
    @Id
    @Column(name = "codigo", nullable = false)
    private Long codigo;

    /**
     * Atributo que obtiene o almacena el nombre del apoyo academico especial
     */
    @Column(name = "nombre", length = 100)
    private String nombre;

    /**
     * Atributo que obtiene o almacena la descripcion del apoyo academico especial
     */
    @Column(name = "descripcion", length = 100)
    private String descripcion;
}
