package co.edu.medellin.sem.ocem.esencia.repository.entity;

import co.edu.medellin.sem.ocem.esencia.repository.entity.support.Auditable2;
import lombok.Data;

import javax.persistence.*;

/**
 * Esta clase es para obtener la entidad de la condicion academica del anio anterior del estudiante
 *
 * @author israel.villegas@medellin.gov.co
 */
@Entity
@Data
@Table(name = "eo_tbl_estudiantes_cond_acad_anio_anterior",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"persona_cod", "anio_matricula"})})
public class EstudianteCondicionAcademicaAnioAnterior extends Auditable2 {

    /**
     * Atributo que obtiene o almacena el codigo de la tabla
     */
    @Id
    @Column(name = "codigo", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "eo_seq_estudiantes_cond_acad_anio_anterior")
    @SequenceGenerator(name = "eo_seq_estudiantes_cond_acad_anio_anterior", sequenceName = "eo_seq_estudiantes_cond_acad_anio_anterior", allocationSize = 1)
    private Long codigo;

    /**
     * Atributo que obtiene o almacena el codigo que identifica la persona en el sistema
     */
    @Column(name = "persona_cod", nullable = false)
    private Long personaCod;

    /**
     * Atributo que obtiene o almacena el codigo que identifica el tipo de condicion academica del anio anterior
     */
    @Column(name = "cond_acad_anio_anterior_Cod", nullable = false)
    private Long condicionAcademicaAnioAnteriorCod;

    /**
     * Atributo que obtiene o almacena el anio del registro y se identifica como anio y por base de datos se identifica
     * como anio
     */
    @Column(name = "anio_matricula", nullable = false)
    private Long anioMatricula;

    /**
     * Atributo que obtiene o almacena el mes del mes en formato numero en que es el registro del SIMAT
     */
    @Column(name = "mes_matricula", nullable = false, length = 2)
    private int MesMatricula;
}
