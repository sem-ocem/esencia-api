package co.edu.medellin.sem.ocem.esencia.repository.entity;

import co.edu.medellin.sem.ocem.esencia.repository.entity.support.Auditable2;
import lombok.Data;

import javax.persistence.*;

/**
 * Esta clase es para obtener los regitros que tienen las excepciones al momento de la carga
 *
 * @author israel.villegas@medellin.gov.co
 */

@Entity
@Data
@Table(name = "eo_tbl_errores_registros")
public class ErrorRegistro extends Auditable2 {

    /**
     * Atributo que obtiene o almacena el codigo de la tabla
     */
    @Id
    @Column(name = "codigo", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "eo_seq_errores_registros")
    @SequenceGenerator(name = "eo_seq_errores_registros", sequenceName = "eo_seq_errores_registros", allocationSize = 1)
    private Long codigo;

    /**
     * Atributo que obtiene o almacena el nombre del archivo donde esta el registro que tiene el error
     */
    @Column(name = "nombre_archivo", length = 100)
    private String nombreArchivo;

    /**
     * Atributo que obtiene o almacena el codigo que identifica el registro que esta en algunos de los archivos (SIMAT,
     * DUE, Talento humano)
     */
    @Column(name = "registro_cod")
    private Long registroCod;

    /**
     * Atributo que obtiene o almacena el nombre de la variable de archivo donde se encuentra el error
     */
    @Column(name = "nombre_variable_archivo", length = 100)
    private String nombreVariableArchivo;

    /**
     * Atributo que obtiene o almacena el codigo que identifica el tipo de capacidad
     */
    @Column(name = "traza_error", length = 2000)
    private String trazaError;

    /**
     * Atributo que obtiene o almacena una descripcion del error del registro
     */
    @Column(name = "descripcion", length = 2000)
    private String descripcion;

}
