package co.edu.medellin.sem.ocem.esencia.repository.port;

import co.edu.medellin.sem.ocem.esencia.repository.entity.Comuna;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ComunaRepository extends JpaRepository<Comuna, Long> {

    Optional<Comuna> findByCodigo(Long codigo);
}
