package co.edu.medellin.sem.ocem.esencia.repository.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Esta clase es para obtener la entidad departamento
 *
 * @author israel.villegas@medellin.gov.co
 */
@Entity
@Data
@Table(name = "eo_tbl_departamentos")
public class Departamento {

    /**
     * Atributo que obtiene o almacena el codigo que identifica el departamento
     */
    @Id
    @Column(name = "codigo", nullable = false)
    private Long codigo;

    /**
     * Atributo que obtiene o almacena el nombre del departamento
     */
    @Column(name = "nombre", length = 100)
    private String nombre;

}
