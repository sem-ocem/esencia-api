package co.edu.medellin.sem.ocem.esencia.repository.entity;


import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Esta clase es para obtener la entidad ConsistenciaMatricula donde alamacena la persistencia de los estudiantes en las
 * matriculas
 *
 * @author israel.villegas@medellin.gov.co
 */
@Entity
@Data
@Table(name = "eo_tbl_consistencia_matricula")
public class ConsistenciaMatricula {


    /**
     * Atributo que obtiene o almacena el codigo que identifica el la consistencia de la matricula
     */
    @Id
    @Column(name = "codigo", nullable = false)
    private Long codigo;

    /**
     * Atributo que obtiene o almacena el anio del registro y se identifica como anio y por base de datos se identifica
     * como anio
     */
    @Column(name = "anio", nullable = false)
    private Long anio;

    /**
     * Atributo que obtiene o almacena el codigo que identifica la persona en el sistema
     */
    @Column(name = "persona_cod", nullable = false)
    private Long personaCod;

    /**
     * Atributo que obtiene o almacena el mes del anio en formato numero en que es el registro del SIMAT
     */
    @Column(name = "mes_Formato_Numero", nullable = false, length = 2)
    private int MesFormatoNumero;
  }
