package co.edu.medellin.sem.ocem.esencia.repository.port;

import co.edu.medellin.sem.ocem.esencia.repository.entity.CondicionAcademicaAnioAnterior;
import co.edu.medellin.sem.ocem.esencia.repository.entity.Zona;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CondicionAcademicaAnioAnteriorRepository extends JpaRepository<CondicionAcademicaAnioAnterior, Long> {

}
