package co.edu.medellin.sem.ocem.esencia.repository.port;

import co.edu.medellin.sem.ocem.esencia.repository.entity.ConsistenciaIE;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConsistenciaIERepository extends JpaRepository<ConsistenciaIE, Long> {

    @Query("select c from ConsistenciaIE c where c.mesFormatoNumero = :mesFormatoNumero")
    List<ConsistenciaIE> lastMoth(@Param("mesFormatoNumero") int mesFormatoNumero);
}
