package co.edu.medellin.sem.ocem.esencia.repository.port;

import co.edu.medellin.sem.ocem.esencia.repository.entity.EstudianteSituacionAcademicaAnioAnterior;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EstudianteSituacionAcademicaAnioAnteriorRepository extends JpaRepository<EstudianteSituacionAcademicaAnioAnterior, Long> {

    @Query("select e from EstudianteSituacionAcademicaAnioAnterior e where e.personaCod = :personaCod order by e.codigo desc")
    List<EstudianteSituacionAcademicaAnioAnterior> findByPersonaCod(@Param("personaCod") Long personaCod);

}
