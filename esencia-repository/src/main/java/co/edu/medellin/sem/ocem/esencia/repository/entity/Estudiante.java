package co.edu.medellin.sem.ocem.esencia.repository.entity;

import co.edu.medellin.sem.ocem.esencia.repository.entity.support.Auditable;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * Esta clase es para obtener la entidad anexo
 *
 * @author israel.villegas@medellin.gov.co
 */
@Entity
@Data
@Table(name = "eo_tbl_estudiantes")
public class Estudiante extends Auditable {

    /**
     * Atributo que obtiene o almacena el codigo que identifica la persona en el sistema
     */
    @Id
    @Column(name = "persona_codigo", nullable = false)
    private Long personaCodigo;

    /**
     * Atributo que obtiene o almacena el mes del anio en que es el registro del SIMAT
     */
    @Column(name = "mes", nullable = false, length = 12)
    private String mes;
    /**
     * Atributo que obtiene o almacena el codigo del municipio  y se identifica como numCodigo y por base de datos se
     * identifica como mun_codigo
     */
    @Column(name = "mun_cod", nullable = false)
    private Long numCod;

    /**
     * Atributo que obtiene o almacena el codigo del dane  y se identifica como daneCodigo y por base de datos se
     * identifica como dane_codigo
     */
    @Column(name = "dane_codigo", nullable = false, length = 12)
    private String daneCodigo;

    /**
     * Atributo que obtiene o almacena el dane anterior y se identifica como daneAnterior y por base de datos
     * se identifica como dane_anterior
     */
    @Column(name = "dane_anterior", nullable = false)
    private Long daneAnterior;

    /**
     * Atributo que obtiene o almacena el consecutivo de la sede y se identifica como sedeConsecutivo y por base de
     * datos se identifica como sede_consecutivo
     */
    @Column(name = "sede_consecutivo", nullable = false, length = 14)
    private String sedeConsecutivo;

    /**
     * Atributo que obtiene o almacena el tipo de documento y se identifica como tipoDocumentoCod y por base de
     * datos se identifica como el tipo_documento_cod
     */
    @Column(name = "tipo_documento_cod", nullable = false)
    private Long tipoDocumentoCod;

    /**
     * Atributo que obtiene o almacena el numero del documento y se identifica como numDocumento y por base de
     * datos se identifica como num_documento
     */
    @Column(name = "num_documento", nullable = false, length = 20)
    private String numDocumento;


    /**
     * Atributo que obtiene o almacena el codigo de expedicion departamento y se identifica como expDepartamentoCod y
     * por base de datos se identifica como exp_departamento_cod, este atributo es el lugar de expedicion del
     * documento
     */
    @Column(name = "exp_departamento_cod", length = 80)
    private Long expDepartamentoCod;


    /**
     * Atributo que obtiene o almacena el codigo de expedicion municipio y se identifica como expMunicipioCod y
     * por base de datos se identifica como exp_municipio_cod,  este atributo es el lugar de expedicion del
     * documento
     */
    @Column(name = "exp_municipio_cod", length = 80)
    private Long expMunicipioCod;

    /**
     * Atributo que obtiene o almacena el primer apellido y se identifica como apellido1 y por base de datos se
     * identifica como apellido1
     */
    @Column(name = "apellido1", nullable = false, length = 80)
    private String apellido1;

    /**
     * Atributo que obtiene o almacena el segundo apellido y se identifica como apellido2 y por base de datos se
     * identifica como apellido2
     */
    @Column(name = "apellido2", length = 80)
    private String apellido2;

    /**
     * Atributo que obtiene o almacena el primer nombre y se identifica como nombre1 y por base de datos se
     * identifica como nombre1
     */
    @Column(name = "nombre1", nullable = false, length = 80)
    private String nombre1;

    /**
     * Atributo que obtiene o almacena el segundo nombre y se identifica como nombre2 y por base de datos se
     * identifica como nombre2
     */
    @Column(name = "nombre2", length = 80)
    private String nombre2;


    /**
     * Atributo que obtiene o almacena el telefono del estudiante y se identifica como telefono y por base de datos se
     * identifica como telefono
     */
    @Column(name = "telefono", length = 50)
    private String telefono;

    /**
     * Atributo que obtiene o almacena el nombre de la residencia del departamento y se identifica como
     * resDepartamentoCod y  por base de datos se identifica como res_departamento_cod y es el nombre del
     * departamento de su residencia donde vive
     */
    @Column(name = "res_departamento_cod", length = 80)
    private Long resDepartamentoCod;

    /**
     * Atributo que obtiene o almacena el nombre de la residencia del municipio y se identifica como
     * resMunicipioCod y  por base de datos se identifica como res_municipio_cod y es el nombre del
     * municipio de su residencia donde vive
     */
    @Column(name = "res_municipio_cod", length = 80)
    private Long resMunicipioCod;

    /**
     * Atributo que obtiene o almacena el codigo del estrato y se identifica como estrato y  por base de datos se
     * identifica como estrato
     */
    @Column(name = "estrato")
    private Long estrato;

    /**
     * Atributo que obtiene o almacena el sisben y se identifica como sisben y  por base de datos se identifica como
     * sisben
     */
    @Column(name = "sisben")
    private String sisben;

    /**
     * Atributo que obtiene o almacena la fecha de nacimiento del estudiante y se identifica como fechaNacimiento y por
     * base de datos se identifica como fecha_nacimiento
     */
    @Column(name = "fecha_nacimiento", nullable = false)
    private Date fechaNacimiento;


    /**
     * Atributo que obtiene o almacena el nombre de la nacionalidad del departamento y se identifica como
     * nacDepartamentoCod y  por base de datos se identifica como nac_departamento_cod
     */
    @Column(name = "nac_departamento_cod", length = 80)
    private Long nacDepartamentoCod;

    /**
     * Atributo que obtiene o almacena el nombre de la nacionalidad del municipio y se identifica como
     * nacMunicipioCod y por base de datos se identifica como nac_municipio_cod
     */
    @Column(name = "nac_municipio_cod", length = 80)
    private Long nacMunicipioCod;

    /**
     * Atributo que obtiene o almacena el codigo del genero y se identifica como generoCodigo y  por base de datos se
     * identifica como genero_codigo
     */
    @Column(name = "genero", nullable = false)
    private String genero;

    /**
     * Atributo que obtiene o almacena el nombre de la poblacion victima del conflicto se identifica como
     * pobVictConfCod y  por base de datos se identifica como pob_vict_conf_cod
     */
    @Column(name = "pob_vict_conf_cod")
    private Long pobVictConfCod;


    /**
     * Atributo que obtiene o almacena el nombre del departamento donde fue victima del conflicto y se identifica como
     * expulDepartamentoCod y  por base de datos se identifica como expul_departamento_cod
     */
    @Column(name = "expul_departamento_cod", length = 80)
    private Long expulDepartamentoCod;


    /**
     * Atributo que obtiene o almacena el nombre del municipio donde fue victima del conflicto se identifica como
     * expulMunicipioCod y  por base de datos se identifica como expul_municipio_cod
     */
    @Column(name = "expul_municipio_cod", length = 80)
    private Long expulMunicipioCod;

    /**
     * Atributo que obtiene o almacena si el estudiante viene de un sector privado y se identifica como
     * provieneSectorPriv y por base de datos se identifica como proviene_sector_priv
     */
    @Column(name = "proviene_sector_priv")
    private String provieneSectorPriv;

    /**
     * Atributo que obtiene o almacena si el estudiante viene de otro municipioy se identifica como provieneOtrMun y por
     * base de datos se identifica como proviene_otr_mun
     */
    @Column(name = "proviene_otr_mun")
    private String provieneOtrMun;


    /**
     * Atributo que obtiene o almacena el nombre de la discapacidad del estudiante y se identifica como
     * discapacidadCod y  por base de datos se identifica como discapacidad_cod
     */
    @Column(name = "discapacidad_cod")
    private Long discapacidadCod;

    /**
     * Atributo que obtiene o almacena el nombre de la capacidad excepcional del estudiante y se identifica como
     * capacidadCod y  por base de datos se identifica como capacidad_cod
     */
    @Column(name = "capacidad_cod")
    private Long capacidadCod;

    /**
     * Atributo que obtiene o almacena el nombre de la etnia del estudiante y se identifica como etniaCod y  por
     * base de datos se identifica como etnia_cod
     */
    @Column(name = "etnia_cod")
    private Long etniaCod;

    /**
     * Atributo que obtiene o almacena el codigo del resguardo del estudiante y se identifica como resguardoCod y por
     * base de datos se identifica como resguardo_cod
     */
    @Column(name = "resguardo_cod")
    private Long resguardoCod;

    @Column(name = "ins_familiar", length = 100)
    private String insFamiliar;

    /**
     * Atributo que obtiene o almacena el codigo de la jornada del estudiante y se identifica como jornadaCod y por
     * base de datos se identifica como jornada_cod
     */
    @Column(name = "jornada_cod")
    private Long jornadaCod;


    /**
     * Atributo que obtiene o almacena el codigo del caracter del estudiante y se identifica como caracterCod y por
     * base de datos se identifica como caracter_cod
     */
    @Column(name = "caracter_cod")
    private Long caracterCod;

    /**
     * Atributo que obtiene o almacena el codigo de la especialidad del estudiante y se identifica como
     * especialidadCod y por base de datos se identifica como especialidad_cod
     */
    @Column(name = "especialidad_cod")
    private Long especialidadCod;

    /**
     * Atributo que obtiene o almacena el codigo del grado del estudiante y se identifica como gradoCod y por base
     * de datos se identifica como grado_cod
     */
    @Column(name = "grado_cod")
    private Long gradoCod;

    /**
     * Atributo que obtiene o almacena el grupo del estudiante y se identifica como grupo y por base de datos se
     * identifica como grupo
     */
    @Column(name = "grupo", length = 10)
    private String grupo;


    /**
     * Atributo que obtiene o almacena el nombre de la metodologia que le ofrecen al estudiante y se identifica como
     * metodologiaCod y por base de datos se identifica como metodologia_cod
     */
    @Column(name = "metodologia_cod", length = 100)
    private Long metodologiaCod;


    /**
     * Atributo que obtiene o almacena si el estudiante tiene matricula contratada y se identifica como
     * matriculaContratada y por base de datos se identifica como matricula_contratada
     */
    @Column(name = "matricula_contratada")
    private String matriculaContratada;

    /**
     * Atributo que obtiene o almacena si el estudiante es repitente y se identifica como repitente y por base de datos
     * se identifica como repitente
     */
    @Column(name = "repitente")
    private String repitente;

    /**
     * Atributo que obtiene o almacena si el estudiante es nuevo y se identifica como nuevo y por base de datos
     * se identifica como nuevo
     */
    @Column(name = "nuevo")
    private String nuevo;

    /**
     * Atributo que obtiene o almacena el codigo de la situacion academica del anio anterior del estudiante y se
     * identifica como sitAcadAnioAnteriorCod y por base de datos se identifica como sit_acad_anio_anterior_cod
     */
    @Column(name = "sit_acad_anio_anterior_cod")
    private Long sitAcadAnioAnteriorCod;


    /**
     * Atributo que obtiene o almacena el codigo de la condicion academica del anio anterior del estudiante y se
     * identifica como condAcadAnioAnteriorCod y por base de datos se identifica como cond_acad_anio_anterior_cod
     */
    @Column(name = "cond_acad_anio_anterior_cod")
    private Long condAcadAnioAnteriorCod;


    /**
     * Atributo que obtiene o almacena el codigo de la fuente de recursos del estudiante y se identifica como
     * fuenteRecursosCod y por base de datos se identifica como fuente_recursos_cod
     */
    @Column(name = "fuente_recursos_cod")
    private Long fuenteRecursosCod;

    /**
     * Atributo que obtiene o almacena el codigo de la zona del estudiante y se identifica como zonaCod y por base de
     * datos se identifica como zona_cod
     */
    @Column(name = "zona_cod")
    private Long zonaCod;

    /**
     * Atributo que obtiene o almacena si el estudiante es una madre cabeza de familia y se identifica como
     * madreCabezaFamilia y por base de datos se identifica como madre_cabeza_familia
     */
    @Column(name = "madre_cabeza_familia")
    private String madreCabezaFamilia;

    /**
     * Atributo que obtiene o almacena si el estudiante pertenece a madre cabeza de familia y se identifica como
     * hijoMadreCabezaFamilia y por base de datos se identifica como hijo_madre_cabeza_familia
     */
    @Column(name = "hijo_madre_cabeza_familia")
    private String hijoMadreCabezaFamilia;

    /**
     * HIXRAEL
     */
    @Column(name = "veterano")
    private String veterano;

    /**
     * HIXRAEL
     */
    @Column(name = "heroe")
    private String heroe;

    /**
     * Atributo que obtiene o almacena el codigo del internado del estudiante y se identifica como internadoCod y por
     * base de datos se identifica como internado_cod
     */
    @Column(name = "internado_cod")
    private Long internadoCod;

    /**
     * HIXRAEL
     */
    @Column(name = "codigo_valoracion1", length = 10)
    private String codigoValoracion1;

    /**
     * HIXRAEL
     */
    @Column(name = "codigo_valoracion2", length = 10)
    private String codigoValoracion2;

    /**
     * HIXRAEL
     */
    @Column(name = "num_convenio", length = 10)
    private String numConvenio;

    /**
     * Atributo que obtiene o almacena el nombre del apoyo academico especial del estudiante y se identifica como
     * apoyoAcademicoEspecialCod  y por base de datos se identifica como apoyo_acad_especial_cod
     */
    @Column(name = "apoyo_acad_especial_cod")
    private Long apoyoAcademicoEspecialCod;

    /**
     * Atributo que obtiene o almacena el nombre del sistema de responsabilidad penal del estudiante y se identifica
     * como sistemaRespPenalCod  y por base de datos se identifica como sist_resp_penal_cod
     */
    @Column(name = "sist_resp_penal_cod")
    private Long sistemaRespPenalCod;

    /**
     * Atributo que obtiene o almacena el nombre del pais de origen del estudiante y se identifica como paisOrigenCod
     * y por base de datos se identifica como pais_origen_cod
     */
    @Column(name = "pais_origen_cod", length = 100)
    private Long paisOrigenCod;

    /**
     * Atributo que obtiene o almacena el codigo de trastornos especificos que tiene el estudiante y se identifica como
     * trastornosEspecificosCod y por base de datos se identifica como trastornos_especificos_cod
     */
    @Column(name = "trastornos_especificos_cod")
    private Long trastornosEspecificosCod;


    /**
     * Atributo que obtiene o almacena el sector (oficial, cobertura contratada o privado) del estudiante y se
     * identifica como sector y en la base de datos se identifica como sector
     */
    @Column(name = "sector")
    private String sector;


    /**
     * Atributo que obtiene el objeto consistenciaMatricula pero es guardado en la bases de datos
     */
    @Transient
    private ConsistenciaMatricula consistenciaMatricula;

    /**
     * Atributo que obtiene el objeto EstudianteVictimaConflicto pero no es guardado en la bases de datos
     */
    @Transient
    private EstudianteVictimaConflicto estudianteVictimaConflicto;

    /**
     * Atributo que obtiene el objeto EstudianteApoyoAcademicoEspecial pero no es guardado en la bases de datos
     */
    @Transient
    private EstudianteApoyoAcademicoEspecial estudianteApoyoAcademicoEspecial;

    /**
     * Atributo que obtiene el objeto EstudianteTipoCapacidad pero no es guardado en la bases de datos
     */
    @Transient
    private EstudianteTipoCapacidad estudianteTipoCapacidad;

    /**
     * Atributo que obtiene el objeto EstudianteZona pero no es guardado en la bases de datos
     */
    @Transient
    private EstudianteZona estudianteZona;

    /**
     * Atributo que obtiene el objeto EstudianteResidencia pero no es guardado en la bases de datos
     */
    @Transient
    private EstudianteResidencia estudianteResidencia;

    /**
     * Atributo que obtiene el objeto EstudianteExpedicionDocumento pero no es guardado en la bases de datos
     */
    @Transient
    private EstudianteExpedicionDocumento estudianteExpedicionDocumento;

    /**
     * Atributo que obtiene el objeto EstudianteExpedicionDocumento pero no es guardado en la bases de datos
     */
    @Transient
    private EstudianteFuenteRecurso estudianteFuenteRecurso;

    /**
     * Atributo que obtiene el objeto estudianteRespPenal pero no es guardado en la bases de datos
     */
    @Transient
    private EstudianteRespPenal estudianteRespPenal;

    /**
     * Atributo que obtiene el objeto estudianteTrastornoEspecifico pero no es guardado en la bases de datos
     */
    @Transient
    private EstudianteTrastornoEspecifico estudianteTrastornoEspecifico;

    /**
     * Atributo que obtiene el objeto estudianteEspecialidad pero no es guardado en la bases de datos
     */
    @Transient
    private EstudianteEspecialidad estudianteEspecialidad;

    /**
     * Atributo que obtiene el objeto estudianteSituacionAcademicaAnioAnterior pero no es guardado en la bases de datos
     */
    @Transient
    private EstudianteSituacionAcademicaAnioAnterior estudianteSituacionAcademicaAnioAnterior;

    /**
     * Atributo que obtiene el objeto estudianteCondicionAcademicaAnioAnterior pero no es guardado en la bases de datos
     */
    @Transient
    private EstudianteCondicionAcademicaAnioAnterior estudianteCondicionAcademicaAnioAnterior;

    /**
     * Atributo que obtiene el objeto estudianteCaracter pero no es guardado en la bases de datos
     */
    @Transient
    private EstudianteCaracter estudianteCaracter;

    /**
     * Atributo que obtiene el objeto estudianteGrado pero no es guardado en la bases de datos
     */
    @Transient
    private EstudianteGrado estudianteGrado;

    /**
     * Atributo que obtiene el objeto gradoMetodologia pero no es guardado en la bases de datos
     */
    @Transient
    private GradoMetodologia gradoMetodologia;
}
