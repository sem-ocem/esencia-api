package co.edu.medellin.sem.ocem.esencia.repository.port;

import co.edu.medellin.sem.ocem.esencia.repository.entity.EstudianteRespPenal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EstudianteRespPenalesRepository extends JpaRepository<EstudianteRespPenal, Long> {

    @Query("select e from EstudianteRespPenal e where e.personaCod = :personaCod order by e.codigo desc")
    List<EstudianteRespPenal> findByPersonaCod(@Param("personaCod") Long personaCod);
}
