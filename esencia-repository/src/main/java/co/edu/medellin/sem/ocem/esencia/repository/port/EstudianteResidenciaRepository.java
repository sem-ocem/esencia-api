package co.edu.medellin.sem.ocem.esencia.repository.port;

import co.edu.medellin.sem.ocem.esencia.repository.entity.EstudianteResidencia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EstudianteResidenciaRepository extends JpaRepository<EstudianteResidencia, Long> {

    EstudianteResidencia findByPersonaCodAndMunicipioCodAndDireccion(Long personaCod, Long municipioCod, String direccion);

    @Query("select e from EstudianteResidencia e where e.personaCod = :personaCod and e.municipioCod = :municipioCod order by e.codigo desc")
    List<EstudianteResidencia> findByPersonaCodAndMunicipioCodList(@Param("personaCod") Long personaCod, @Param("municipioCod") Long municipioCod);
}
