package co.edu.medellin.sem.ocem.esencia.repository.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Esta clase es para obtener los tipos de documento
 *
 * @author israel.villegas@medellin.gov.co
 */
@Entity
@Data
@Table(name = "eo_tbl_tipos_documentos")
public class TipoDocumento {

    /**
     * Atributo que obtiene o almacena el codigo que identifica el tipo de documento
     */
    @Id
    @Column(name = "codigo", nullable = false)
    private Long codigo;

    /**
     * Atributo que obtiene o almacena la sigla del tipo de documento
     */
    @Column(name = "siglas", length = 50)
    private String siglas;

    /**
     * Atributo que obtiene o almacena el nombre del tipo de documento
     */
    @Column(name = "nombre", length = 100)
    private String nombre;

    /**
     * Atributo que obtiene o almacena la descripcion del tipo de documento
     */
    @Column(name = "descripcion", length = 100)
    private String descripcion;
}
