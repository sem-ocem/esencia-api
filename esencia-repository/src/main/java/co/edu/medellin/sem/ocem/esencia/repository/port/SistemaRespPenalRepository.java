package co.edu.medellin.sem.ocem.esencia.repository.port;

import co.edu.medellin.sem.ocem.esencia.repository.entity.SistemaRespPenal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface SistemaRespPenalRepository extends JpaRepository<SistemaRespPenal, Long> {

    Optional<SistemaRespPenal> findByCodigo(Long codigo);
}
