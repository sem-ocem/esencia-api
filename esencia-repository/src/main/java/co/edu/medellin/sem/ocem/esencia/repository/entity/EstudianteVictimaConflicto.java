package co.edu.medellin.sem.ocem.esencia.repository.entity;

import co.edu.medellin.sem.ocem.esencia.repository.entity.support.Auditable;
import lombok.Data;

import javax.persistence.*;

/**
 * Esta clase es para obtener los conflictos que puede tener los estudiantes
 *
 * @author israel.villegas@medellin.gov.co
 */
@Entity
@Data
@Table(name = "eo_tbl_estudiantes_victimas_conflicto",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"anio_matricula", "mes_matricula", "persona_cod"})})
public class EstudianteVictimaConflicto extends Auditable {

    /**
     * Atributo que obtiene o almacena el codigo de la tabla
     */
    @Id
    @Column(name = "codigo", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "eo_seq_estudiantes_victimas_conflicto")
    @SequenceGenerator(name = "eo_seq_estudiantes_victimas_conflicto", sequenceName = "eo_seq_estudiantes_victimas_conflicto", allocationSize = 1)
    private Long codigo;

    /**
     * Atributo que obtiene o almacena el codigo que identifica la persona en el sistema
     */
    @Column(name = "persona_cod", nullable = false)
    private Long personaCod;


    /**
     * Atributo que obtiene o almacena el codigo del municipio que viene del archivo plano SIMAT
     */
    @Column(name = "municipio_cod")
    private Long municipioCod;


    /**
     * Atributo que obtiene o almacena el codigo que identifica el conflicto
     */
    @Column(name = "victima_conflicto_cod", nullable = false)
    private Long victimaConflictoCod;

    /**
     * Atributo que obtiene o almacena el anio del registro y se identifica como anio y por base de datos se identifica
     * como anio
     */
    @Column(name = "anio_matricula", nullable = false)
    private Long anioMatricula;


    /**
     * Atributo que obtiene o almacena el mes del anio en formato numero en que es el registro del SIMAT
     */
    @Column(name = "mes_matricula", nullable = false, length = 2)
    private int MesMatricula;


}
