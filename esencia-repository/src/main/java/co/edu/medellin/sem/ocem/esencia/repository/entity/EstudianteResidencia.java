package co.edu.medellin.sem.ocem.esencia.repository.entity;

import co.edu.medellin.sem.ocem.esencia.repository.entity.support.Auditable2;
import lombok.Data;

import javax.persistence.*;

/**
 * Esta clase es para obtener las zonas que puede tener los estudiantes
 *
 * @author israel.villegas@medellin.gov.co
 */

@Entity
@Data
@Table(name = "eo_tbl_estudiantes_residencias")
public class EstudianteResidencia extends Auditable2 {

    /**
     * Atributo que obtiene o almacena el codigo de la tabla
     */
    @Id
    @Column(name = "codigo", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "eo_seq_estudiantes_recidencias")
    @SequenceGenerator(name = "eo_seq_estudiantes_recidencias", sequenceName = "eo_seq_estudiantes_recidencias", allocationSize = 1)
    private Long codigo;

    /**
     * Atributo que obtiene o almacena el codigo que identifica la persona (Estudiante) en el sistema
     */
    @Column(name = "persona_cod", nullable = false)
    private Long personaCod;

    /**
     * Atributo que obtiene o almacena el codigo del municipio que viene del archivo plano SIMAT
     */
    @Column(name = "municipio_cod", nullable = false)
    private Long municipioCod;

    /**
     * Atributo que obtiene o almacena la residencia del estudiante y se identifica como direccion y por base
     * de datos se identifica como direccion
     */
    @Column(name = "direccion", length = 100)
    private String direccion;


}
