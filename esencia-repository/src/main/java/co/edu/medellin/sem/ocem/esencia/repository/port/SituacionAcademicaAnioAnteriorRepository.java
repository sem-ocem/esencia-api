package co.edu.medellin.sem.ocem.esencia.repository.port;

import co.edu.medellin.sem.ocem.esencia.repository.entity.SituacionAcademicaAnioAnterior;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SituacionAcademicaAnioAnteriorRepository extends JpaRepository<SituacionAcademicaAnioAnterior, Long> {

}
