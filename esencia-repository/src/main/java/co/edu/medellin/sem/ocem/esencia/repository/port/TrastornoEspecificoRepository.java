package co.edu.medellin.sem.ocem.esencia.repository.port;

import co.edu.medellin.sem.ocem.esencia.repository.entity.TrastornoEspecifico;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TrastornoEspecificoRepository extends JpaRepository<TrastornoEspecifico, Long> {

    Optional<TrastornoEspecifico> findByCodigo(Long codigo);
}
