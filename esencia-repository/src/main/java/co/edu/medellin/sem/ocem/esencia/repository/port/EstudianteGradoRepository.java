package co.edu.medellin.sem.ocem.esencia.repository.port;

import co.edu.medellin.sem.ocem.esencia.repository.entity.EstudianteEspecialidad;
import co.edu.medellin.sem.ocem.esencia.repository.entity.EstudianteGrado;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EstudianteGradoRepository extends JpaRepository<EstudianteGrado, Long> {

    @Query("select e from EstudianteGrado e where e.personaCod = :personaCod order by e.codigo desc")
    List<EstudianteGrado> findByPersonaCod(@Param("personaCod") Long personaCod);
}
