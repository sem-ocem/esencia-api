package co.edu.medellin.sem.ocem.esencia.repository.port;

import co.edu.medellin.sem.ocem.esencia.repository.entity.TipoDiscapacidad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TipoDiscapacidadRepository extends JpaRepository<TipoDiscapacidad, Long> {

    Optional<TipoDiscapacidad> findByCodigo(Long codigo);
}
