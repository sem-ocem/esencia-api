package co.edu.medellin.sem.ocem.esencia.repository.port;

import co.edu.medellin.sem.ocem.esencia.repository.entity.TipoCapacidad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TipoCapacidadRepository extends JpaRepository<TipoCapacidad, Long> {

    Optional<TipoCapacidad> findByCodigo(Long codigo);
}
