package co.edu.medellin.sem.ocem.esencia.repository.entity;


import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Esta clase es para obtener la entidad de Zona division politica admin
 *
 * @author israel.villegas@medellin.gov.co
 */
@Entity
@Data
@Table(name = "oe_tbl_zonas_divisiones_politicas_admin")
public class ZonaDivisionPoliticaAdmin {

    /**
     * Atributo que obtiene o almacena el codigo que identifica la zona division politica admin
     */
    @Id
    @Column(name = "codigo", nullable = false)
    private Long codigo;

    /**
     * Atributo que obtiene o almacena el nombre de la zona division politica admin
     */
    @Column(name = "nombre", length = 100)
    private String nombre;

    /**
     * Atributo que obtiene o almacena la descripcion de la zona division politica admin
     */
    @Column(name = "descripcion", length = 100)
    private String descripcion;
}
