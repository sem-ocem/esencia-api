package co.edu.medellin.sem.ocem.esencia.repository.port;

import co.edu.medellin.sem.ocem.esencia.repository.entity.EstudianteVictimaConflicto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EstudianteVictimaConflictoRepository extends JpaRepository<EstudianteVictimaConflicto, Long> {

}
