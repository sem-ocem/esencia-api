
CREATE SEQUENCE ocem.eo_seq_estudiantes_victimas_conflicto
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807

-- Permissions

ALTER SEQUENCE ocem.eo_seq_estudiantes_victimas_conflicto OWNER TO postgres;
GRANT ALL ON SEQUENCE ocem.eo_seq_estudiantes_victimas_conflicto TO postgres;