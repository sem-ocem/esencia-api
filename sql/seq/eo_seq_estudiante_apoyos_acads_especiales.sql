-- DROP SEQUENCE eo_seq_estudiantes_apoyos_acads_especiales;

CREATE SEQUENCE ocem.eo_seq_estudiante_apoyos_acads_especiales
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807

-- Permissions

ALTER SEQUENCE ocem.eo_seq_estudiante_apoyos_acads_especiales OWNER TO postgres;
GRANT ALL ON SEQUENCE ocem.eo_seq_estudiante_apoyos_acads_especiales TO postgres;
