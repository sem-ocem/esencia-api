-- DROP SEQUENCE ocem.eo_seq_estudiantes_tipos_capacidad;

CREATE SEQUENCE ocem.eo_seq_estudiantes_tipos_capacidad
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807

-- Permissions

ALTER SEQUENCE ocem.eo_seq_estudiantes_tipos_capacidad OWNER TO postgres;
GRANT ALL ON SEQUENCE ocem.eo_seq_estudiantes_tipos_capacidad TO postgres;