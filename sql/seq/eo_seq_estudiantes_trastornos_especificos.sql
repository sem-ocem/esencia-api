-- DROP SEQUENCE eo_seq_estudiantes_trastornos_especificos;

CREATE SEQUENCE eo_seq_estudiantes_trastornos_especificos
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807

-- Permissions

ALTER SEQUENCE eo_seq_estudiantes_trastornos_especificos OWNER TO postgres;
GRANT ALL ON SEQUENCE eo_seq_estudiantes_trastornos_especificos TO postgres;
