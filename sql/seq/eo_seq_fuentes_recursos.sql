-- DROP SEQUENCE ocem.eo_seq_fuentes_recursos;

CREATE SEQUENCE eo_seq_fuentes_recursos
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807

-- Permissions

ALTER SEQUENCE eo_seq_fuentes_recursos OWNER TO postgres;
GRANT ALL ON SEQUENCE eo_seq_fuentes_recursos TO postgres;