-- DROP SEQUENCE ocem.eo_seq_municipios;

CREATE SEQUENCE ocem.eo_seq_municipios
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807

-- Permissions

ALTER SEQUENCE ocem.eo_seq_municipios OWNER TO postgres;
GRANT ALL ON SEQUENCE ocem.eo_seq_municipios TO postgr