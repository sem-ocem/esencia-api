-- DROP SEQUENCE eo_seq_estudiantes_expedicion_documento;

CREATE SEQUENCE ocem.eo_seq_estudiantes_expedicion_documento
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807

-- Permissions

ALTER SEQUENCE eo_seq_estudiantes_expedicion_documento OWNER TO postgres;
GRANT ALL ON SEQUENCE eo_seq_estudiantes_expedicion_documento TO postgres;