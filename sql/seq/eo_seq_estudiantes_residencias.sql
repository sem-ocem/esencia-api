-- DROP SEQUENCE eo_seq_estudiantes_recidencias;

CREATE SEQUENCE ocem.eo_seq_estudiantes_recidencias
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807

-- Permissions

ALTER SEQUENCE ocem.eo_seq_estudiantes_recidencias OWNER TO postgres;
GRANT ALL ON SEQUENCE ocem.eo_seq_estudiantes_recidencias TO postgres;