-- DROP SEQUENCE eo_seq_estudiantes_responsabilidades_penales;

CREATE SEQUENCE ocem.eo_seq_estudiantes_responsabilidades_penales
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807

-- Permissions

ALTER SEQUENCE eo_seq_estudiantes_responsabilidades_penales OWNER TO postgres;
GRANT ALL ON SEQUENCE eo_seq_estudiantes_responsabilidades_penales TO postgres;