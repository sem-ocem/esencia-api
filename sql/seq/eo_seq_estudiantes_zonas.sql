-- DROP SEQUENCE ocem.eo_seq_estudiantes_zonas;

CREATE SEQUENCE ocem.eo_seq_estudiantes_zonas
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807

-- Permissions

ALTER SEQUENCE ocem.eo_seq_estudiantes_zonas OWNER TO postgres;
GRANT ALL ON SEQUENCE ocem.eo_seq_estudiantes_zonas TO postgres;
