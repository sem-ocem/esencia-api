-- Drop table

-- DROP TABLE eo_tbl_estudiantes_expedicion_documento;

CREATE TABLE ocem.eo_tbl_estudiantes_expedicion_documento (
	codigo int8 NOT NULL,
	fe_creado timestamp NULL,
	municipio_cod int8 NOT NULL,
	persona_cod int8 NOT NULL,
	CONSTRAINT eo_tbl_estudiantes_expedicion_documento_pkey PRIMARY KEY (codigo),
	CONSTRAINT ukawv1wsp1tl845x5go8h5fes91 UNIQUE (persona_cod, municipio_cod),
	CONSTRAINT eo_tbl_estudiantes_expedicion_documento_fk FOREIGN KEY (persona_cod) REFERENCES eo_tbl_estudiantes(persona_codigo),
	CONSTRAINT eo_tbl_estudiantes_expedicion_documento_fk_1 FOREIGN KEY (municipio_cod) REFERENCES eo_tbl_municipios(codigo)
);