CREATE TABLE ocem.eo_tbl_departamentos (
	codigo int8 NOT NULL,
	nombre varchar(100) NULL,
	CONSTRAINT eo_tbl_departamentos_pkey PRIMARY KEY (codigo)
);

CREATE TABLE ocem.eo_tbl_municipios (
	codigo int8 NOT NULL,
	departamento_codigo int8 NOT NULL,
	nombre varchar(100) NULL,
	CONSTRAINT eo_tbl_municipios_pk PRIMARY KEY (codigo, departamento_codigo)
);

CREATE TABLE ocem.eo_tbl_estudiantes (
	persona_codigo int8 NOT NULL,
	apellido1 varchar(80) NOT NULL,
	apellido2 varchar(80) NULL,
	apoyo_acad_especial int8 NULL,
	capacidad_codigo int8 NULL,
	caracter_codigo int8 NULL,
	codigo_valoracion1 varchar(10) NULL,
	codigo_valoracion2 varchar(10) NULL,
	cond_acad_anio_anterior_codigo int8 NULL,
	dane_anterior varchar(12) NOT NULL,
	dane_codigo varchar(12) NOT NULL,
	direccion_residencia varchar(100) NULL,
	discapacidad_codigo int8 NULL,
	especialidad_codigo int8 NULL,
	estrato int8 NULL,
	etnia_codigo int8 NULL,
	exp_departamento varchar(80) NULL,
	exp_municipio varchar(80) NULL,
	expul_departamento varchar(80) NULL,
	expul_municipio varchar(80) NULL,
	fecha_nacimiento timestamp NOT NULL,
	fuente_recursos_codigo varchar(255) NULL,
	genero varchar(255) NOT NULL,
	grado_codigo int8 NULL,
	grupo varchar(10) NULL,
	heroe varchar(255) NULL,
	hijo_madre_cabeza_familia varchar(255) NULL,
	ins_familiar varchar(100) NULL,
	internado_codigo int8 NULL,
	jornada_codigo int8 NULL,
	madre_cabeza_familia varchar(255) NULL,
	matricula_contratada varchar(255) NULL,
	mes varchar(12) NOT NULL,
	metodologia_codigo int8 NULL,
	nac_departamento varchar(80) NULL,
	nac_municipio varchar(80) NULL,
	nombre1 varchar(80) NOT NULL,
	nombre2 varchar(80) NULL,
	nuevo varchar(255) NULL,
	mun_codigo int8 NOT NULL,
	num_convenio varchar(10) NULL,
	num_documento varchar(20) NOT NULL,
	pais_origen_codigo int8 NULL,
	pob_vict_conf varchar(255) NULL,
	proviene_otr_mun varchar(255) NULL,
	proviene_sector_priv varchar(255) NULL,
	repitente varchar(255) NULL,
	res_departamento varchar(80) NULL,
	res_municipio varchar(80) NULL,
	resguardo_codigo int8 NULL,
	sede_consecutivo varchar(14) NOT NULL,
	sisben varchar(255) NULL,
	sist_resp_penal int8 NULL,
	sit_acad_anio_anterior_codigo int8 NULL,
	telefono varchar(50) NULL,
	tipo_documento varchar(255) NOT NULL,
	trastornos_especificos_codigo int8 NULL,
	veterano varchar(255) NULL,
	zona_codigo varchar(255) NULL,
	CONSTRAINT eo_tbl_estudiantes_pkey PRIMARY KEY (persona_codigo)
);

CREATE TABLE ocem.eo_tbl_consistencia_matricula (
	mes_formato_numero int4 NOT NULL,
	anio int8 NOT NULL,
	persona_codigo int8 NOT NULL,
	CONSTRAINT eo_tbl_consistencia_matricula_pkey PRIMARY KEY (mes_formato_numero, anio, persona_codigo)
);

CREATE TABLE ocem.eo_tbl_ie (
	dane_anterior int8 NOT NULL,
	barrio varchar(255) NULL,
	comuna int8 NOT NULL,
	correo_electronico varchar(600) NULL,
	dane_consecutivo int8 NOT NULL,
	dane_establecimiento int8 NOT NULL,
	direccion varchar(255) NULL,
	nit varchar(255) NULL,
	nombre_comuna varchar(255) NULL,
	nombre_sede varchar(255) NULL,
	nombre_zona varchar(255) NOT NULL,
	nucleo int4 NOT NULL,
	numero_oficial int4 NOT NULL,
	observaciones varchar(600) NULL,
	prestacion_servicio varchar(255) NULL,
	rector varchar(255) NULL,
	sector varchar(255) NULL,
	telefono varchar(255) NULL,
	tipo_sede varchar(255) NULL,
	vereda varchar(255) NULL,
	zona int4 NOT NULL,
	zona_georeferenciado varchar(255) NULL,
	zona_sineb varchar(255) NULL,
	CONSTRAINT eo_tbl_ie_pkey PRIMARY KEY (dane_anterior)
);

CREATE TABLE ocem.eo_tbl_consistencia_ie (
	anio int8 NOT NULL,
	dane_anterior int8 NOT NULL,
	mes_formato_numero int4 NOT NULL,
	CONSTRAINT eo_tbl_consistencia_ie_pkey PRIMARY KEY (anio, dane_anterior, mes_formato_numero)
);

CREATE TABLE ocem.eo_tbl_tipos_documentos (
	codigo int8 NOT NULL,
	descripcion varchar(100) NULL,
	nombre varchar(100) NULL,
	siglas varchar(50) NULL,
	CONSTRAINT eo_tbl_tipos_documentos_pkey PRIMARY KEY (codigo)
);


CREATE TABLE ocem.eo_tbl_tipos_victimas_conflicto (
	codigo int8 NOT NULL,
	descripcion varchar(100) NULL,
	nombre varchar(100) NULL,
	CONSTRAINT eo_tbl_tipos_victimas_conflicto_pkey PRIMARY KEY (codigo)
);


CREATE TABLE ocem.eo_tbl_trastornos_especificos (
	codigo int8 NOT NULL,
	descripcion varchar(100) NULL,
	nombre varchar(100) NULL,
	CONSTRAINT eo_tbl_trastornos_especificos_pkey PRIMARY KEY (codigo)
);

CREATE TABLE ocem.eo_tbl_etnias (
	codigo int8 NOT NULL,
	descripcion varchar(100) NULL,
	nombre varchar(100) NULL,
	CONSTRAINT eo_tbl_etnias_pkey PRIMARY KEY (codigo)
);

CREATE TABLE ocem.eo_tbl_grados (
	codigo int8 NOT NULL,
	descripcion varchar(100) NULL,
	nombre varchar(100) NULL,
	CONSTRAINT eo_tbl_grados_pkey PRIMARY KEY (codigo)
);

CREATE TABLE ocem.eo_tbl_jornadas (
	codigo int8 NOT NULL,
	descripcion varchar(100) NULL,
	nombre varchar(100) NULL,
	CONSTRAINT eo_tbl_jornadas_pkey PRIMARY KEY (codigo)
);

CREATE TABLE ocem.eo_tbl_metodologias (
	codigo int8 NOT NULL,
	descripcion varchar(100) NULL,
	nombre varchar(100) NULL,
	CONSTRAINT eo_tbl_metodologias_pkey PRIMARY KEY (codigo)
);

CREATE TABLE ocem.eo_tbl_paises_origen (
	codigo int8 NOT NULL,
	descripcion varchar(100) NULL,
	nombre varchar(100) NULL,
	CONSTRAINT eo_tbl_paises_origen_pkey PRIMARY KEY (codigo)
);

CREATE TABLE ocem.eo_tbl_sistemas_resp_penal (
	codigo int8 NOT NULL,
	descripcion varchar(100) NULL,
	nombre varchar(100) NULL,
	CONSTRAINT eo_tbl_sistemas_resp_penal_pkey PRIMARY KEY (codigo)
);

CREATE TABLE ocem.eo_tbl_tipos_capacidad (
	codigo int8 NOT NULL,
	descripcion varchar(100) NULL,
	nombre varchar(100) NULL,
	CONSTRAINT eo_tbl_tipos_capacidad_pkey PRIMARY KEY (codigo)
);

CREATE TABLE ocem.eo_tbl_tipos_discapacidad (
	codigo int8 NOT NULL,
	descripcion varchar(100) NULL,
	nombre varchar(100) NULL,
	CONSTRAINT eo_tbl_tipos_discapacidad_pkey PRIMARY KEY (codigo)
);














