-- Drop table

-- DROP TABLE eo_tbl_estudiantes_zonas;

CREATE TABLE ocem.eo_tbl_estudiantes_zonas (
	codigo int8 NOT NULL,
	mes_matricula int4 NOT NULL,
	anio_matricula int8 NOT NULL,
	persona_cod int8 NOT NULL,
	zona_cod int8 NOT NULL,
	CONSTRAINT eo_tbl_estudiantes_zonas_pkey PRIMARY KEY (codigo),
	CONSTRAINT ukf8i1sfq1cndnw2gcrmci20erk UNIQUE (persona_cod, anio_matricula, mes_matricula),
	CONSTRAINT eo_tbl_estudiantes_zonas_fk FOREIGN KEY (persona_cod) REFERENCES ocem.eo_tbl_estudiantes(persona_codigo),
	CONSTRAINT eo_tbl_estudiantes_zonas_fk_1 FOREIGN KEY (zona_cod) REFERENCES ocem.eo_tbl_zonas(codigo)
);

