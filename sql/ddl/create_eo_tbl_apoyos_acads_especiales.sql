-- Drop table

-- DROP TABLE ocem.eo_tbl_estudiante_apoyos_acads_especiales;

CREATE TABLE eo_tbl_estudiante_apoyos_acads_especiales (
	codigo int8 NOT NULL,
	mes_matricula int4 NOT NULL,
	anio_matricula int8 NOT NULL,
	apoyo_acad_esp_cod int8 NOT NULL,
	persona_cod int8 NOT NULL,
	CONSTRAINT eo_tbl_estudiante_apoyos_acads_especiales_pkey PRIMARY KEY (codigo),
	CONSTRAINT uk9tv9g4hx7dqkorhyu6mamhjqr UNIQUE (anio_matricula, mes_matricula)
);