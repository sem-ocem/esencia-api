-- Drop table

-- DROP TABLE ocem.eo_tbl_estudiantes_grados;

CREATE TABLE eo_tbl_estudiantes_grados (
	codigo int8 NOT NULL,
	fe_creado timestamp NULL,
	mes_matricula int4 NOT NULL,
	anio_matricula int8 NOT NULL,
	grado_cod int8 NOT NULL,
	persona_cod int8 NOT NULL,
	CONSTRAINT eo_tbl_estudiantes_grados_pkey PRIMARY KEY (codigo),
	CONSTRAINT uk33watr7yyd651u4gtqbf7you0 UNIQUE (persona_cod, anio_matricula, mes_matricula),
	CONSTRAINT eo_tbl_estudiantes_grados_fk FOREIGN KEY (persona_cod) REFERENCES eo_tbl_estudiantes(persona_codigo),
	CONSTRAINT eo_tbl_estudiantes_grados_fk_1 FOREIGN KEY (grado_cod) REFERENCES eo_tbl_grados(codigo)
);

-- Permissions

ALTER TABLE ocem.eo_tbl_estudiantes_grados OWNER TO postgres;
GRANT ALL ON TABLE ocem.eo_tbl_estudiantes_grados TO postgres;
