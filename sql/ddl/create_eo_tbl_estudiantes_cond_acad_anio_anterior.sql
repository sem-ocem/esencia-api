-- Drop table

-- DROP TABLE ocem.eo_tbl_estudiantes_cond_acad_anio_anterior;

CREATE TABLE eo_tbl_estudiantes_cond_acad_anio_anterior (
	codigo int8 NOT NULL,
	fe_creado timestamp NULL,
	anio_matricula int8 NOT NULL,
	persona_cod int8 NOT NULL,
	cond_acad_anio_anterior_cod int8 NOT NULL,
	mes_matricula int4 NOT NULL,
	CONSTRAINT eo_tbl_estudiantes_cond_acad_anio_anterior_pkey PRIMARY KEY (codigo),
	CONSTRAINT ukfi555wptki9wkg2e94lo9vlfl UNIQUE (persona_cod, anio_matricula),
	CONSTRAINT eo_tbl_estudiantes_cond_acad_anio_anterior_fk FOREIGN KEY (persona_cod) REFERENCES eo_tbl_estudiantes(persona_codigo),
	CONSTRAINT eo_tbl_estudiantes_cond_acad_anio_anterior_fk_1 FOREIGN KEY (cond_acad_anio_anterior_cod) REFERENCES eo_tbl_cond_acad_anios_anteriores(codigo)
);