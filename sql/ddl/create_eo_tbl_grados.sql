-- Drop table

-- DROP TABLE ocem.eo_tbl_grados;

CREATE TABLE ocem.eo_tbl_grados (
	codigo int8 NOT NULL,
	descripcion varchar(100) NULL,
	nombre varchar(100) NULL,
	CONSTRAINT eo_tbl_grados_pkey PRIMARY KEY (codigo)
);