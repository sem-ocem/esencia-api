-- Drop table

-- DROP TABLE ocem.eo_tbl_departamentos;

CREATE TABLE ocem.eo_tbl_departamentos (
	codigo int8 NOT NULL,
	nombre varchar(100) NULL,
	CONSTRAINT eo_tbl_departamentos_pkey PRIMARY KEY (codigo)
);
