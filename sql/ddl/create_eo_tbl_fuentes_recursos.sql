-- Drop table

-- DROP TABLE ocem.eo_tbl_fuentes_recursos;

CREATE TABLE ocem.eo_tbl_fuentes_recursos (
	codigo int8 NOT NULL,
	descripcion varchar(100) NULL,
	fuente_recursos_estandar_cod int8 NOT NULL,
	nombre varchar(100) NULL,
	para varchar(50) NULL,
	CONSTRAINT eo_tbl_fuentes_recursos_pkey PRIMARY KEY (codigo)
);

-- Permissions

ALTER TABLE ocem.eo_tbl_fuentes_recursos OWNER TO postgres;
GRANT ALL ON TABLE ocem.eo_tbl_fuentes_recursos TO postgres;
