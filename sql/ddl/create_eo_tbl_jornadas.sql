-- Drop table

-- DROP TABLE ocem.eo_tbl_jornadas;

CREATE TABLE ocem.eo_tbl_jornadas (
	codigo int8 NOT NULL,
	descripcion varchar(100) NULL,
	nombre varchar(100) NULL,
	CONSTRAINT eo_tbl_jornadas_pkey PRIMARY KEY (codigo)
);