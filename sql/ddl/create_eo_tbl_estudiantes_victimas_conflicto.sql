-- Drop table

-- DROP TABLE ocem.eo_tbl_estudiantes_victimas_conflicto;

CREATE TABLE eo_tbl_estudiantes_victimas_conflicto (
	codigo int8 NOT NULL,
	fe_creado timestamp NULL,
	fe_modificado timestamp NULL,
	municipio_cod int8 NOT NULL,
	persona_cod int8 NOT NULL,
	victima_conflicto_cod int8 NOT NULL,
	mes_matricula int4 NOT NULL,
	anio_matricula int8 NOT NULL,
	CONSTRAINT eo_tbl_estudiantes_victimas_conflicto_pkey PRIMARY KEY (codigo),
	CONSTRAINT ukof3gdeu7jqm9ovp6k2yj51q69 UNIQUE (anio_matricula, mes_matricula),
	CONSTRAINT eo_tbl_estudiantes_victimas_conflicto_fk FOREIGN KEY (persona_cod) REFERENCES eo_tbl_estudiantes(persona_codigo),
	CONSTRAINT eo_tbl_estudiantes_victimas_conflicto_fk_2 FOREIGN KEY (victima_conflicto_cod) REFERENCES eo_tbl_tipos_victimas_conflicto(codigo)
);

-- Permissions

ALTER TABLE eo_tbl_estudiantes_victimas_conflicto OWNER TO postgres;
GRANT ALL ON TABLE eo_tbl_estudiantes_victimas_conflicto TO postgres;