-- Drop table

-- DROP TABLE ocem.eo_tbl_tipos_documentos;

CREATE TABLE ocem.eo_tbl_tipos_documentos (
	codigo int8 NOT NULL,
	descripcion varchar(100) NULL,
	nombre varchar(100) NULL,
	siglas varchar(50) NULL,
	CONSTRAINT eo_tbl_tipos_documentos_pkey PRIMARY KEY (codigo)
);