-- Drop table

-- DROP TABLE ocem.eo_tbl_municipios;

CREATE TABLE ocem.eo_tbl_municipios (
	codigo int8 NOT NULL,
	departamento_estandar_cod int8 NULL,
	municipio_estandar_cod int8 NOT NULL,
	nombre varchar(100) NULL,
	CONSTRAINT eo_tbl_municipios_pkey PRIMARY KEY (codigo)
);
