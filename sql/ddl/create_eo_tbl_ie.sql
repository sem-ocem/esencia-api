-- Drop table

-- DROP TABLE eo_tbl_ies;

CREATE TABLE eo_tbl_ies (
	dane_anterior int8 NOT NULL,
	barrio varchar(255) NULL,
	celulares varchar(600) NULL,
	comuna_cod int8 NOT NULL,
	correo_electronico varchar(600) NULL,
	dane_anterior_sector varchar(255) NULL,
	dane_consecutivo int8 NOT NULL,
	dane_establecimiento int8 NOT NULL,
	direccion varchar(255) NULL,
	nit varchar(255) NULL,
	nombre_sede varchar(255) NULL,
	nucleo int4 NOT NULL,
	numero_oficial int4 NOT NULL,
	observaciones varchar(600) NULL,
	prestacion_servicio varchar(255) NULL,
	rector varchar(255) NULL,
	rector_cedula int8 NULL,
	sector varchar(255) NULL,
	telefono varchar(255) NULL,
	tipo_sede varchar(255) NULL,
	vereda varchar(255) NULL,
	zona_division_politica_admin_cod int8 NOT NULL,
	zona_georeferenciado varchar(255) NULL,
	zona_sineb varchar(255) NULL,
	CONSTRAINT eo_tbl_ies_pkey PRIMARY KEY (dane_anterior),
	CONSTRAINT eo_tbl_ies_fk FOREIGN KEY (comuna_cod) REFERENCES eo_tbl_comunas(codigo),
	CONSTRAINT eo_tbl_ies_fk_1 FOREIGN KEY (zona_division_politica_admin_cod) REFERENCES oe_tbl_zonas_divisiones_politicas_admin(codigo)
);

-- Permissions

ALTER TABLE eo_tbl_ies OWNER TO postgres;
GRANT ALL ON TABLE eo_tbl_ies TO postgres;
