-- Drop table

-- DROP TABLE ocem.eo_tbl_trastornos_especificos;

CREATE TABLE ocem.eo_tbl_trastornos_especificos (
	codigo int8 NOT NULL,
	descripcion varchar(100) NULL,
	nombre varchar(100) NULL,
	CONSTRAINT eo_tbl_trastornos_especificos_pkey PRIMARY KEY (codigo)
);

