-- Drop table

-- DROP TABLE ocem.eo_tbl_metodologias;

CREATE TABLE ocem.eo_tbl_metodologias (
	codigo int8 NOT NULL,
	descripcion varchar(100) NULL,
	nombre varchar(100) NULL,
	CONSTRAINT eo_tbl_metodologias_pkey PRIMARY KEY (codigo)
);