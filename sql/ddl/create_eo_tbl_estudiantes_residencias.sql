-- Drop table

-- DROP TABLE ocem.eo_tbl_estudiantes_residencias;

CREATE TABLE ocem.eo_tbl_estudiantes_residencias (
	codigo int8 NOT NULL,
	fe_creado timestamp NULL,
	direccion varchar(100) NULL,
	municipio_cod int8 NOT NULL,
	persona_cod int8 NOT NULL,
	CONSTRAINT eo_tbl_estudiantes_residencias_pkey PRIMARY KEY (codigo),
	CONSTRAINT eo_tbl_estudiantes_residencias_fk FOREIGN KEY (persona_cod) REFERENCES ocem.eo_tbl_estudiantes(persona_codigo),
	CONSTRAINT eo_tbl_estudiantes_residencias_fk_1 FOREIGN KEY (municipio_cod) REFERENCES ocem.eo_tbl_municipios(codigo)
);