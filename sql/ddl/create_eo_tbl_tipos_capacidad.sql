-- Drop table

-- DROP TABLE ocem.eo_tbl_tipos_capacidad;

CREATE TABLE ocem.eo_tbl_tipos_capacidad (
	codigo int8 NOT NULL,
	descripcion varchar(100) NULL,
	nombre varchar(100) NULL,
	CONSTRAINT eo_tbl_tipos_capacidad_pkey PRIMARY KEY (codigo)
);