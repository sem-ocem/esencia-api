-- Drop table

-- DROP TABLE ocem.eo_tbl_estudiantes_responsabilidades_penales;

CREATE TABLE ocem.eo_tbl_estudiantes_responsabilidades_penales (
	codigo int8 NOT NULL,
	fe_creado timestamp NULL,
	persona_cod int8 NOT NULL,
	sistema_resp_penal_cod int8 NOT NULL,
	mes_matricula int4 NOT NULL,
	anio_matricula int8 NOT NULL,
	CONSTRAINT eo_tbl_estudiantes_responsabilidades_penales_pkey PRIMARY KEY (codigo),
	CONSTRAINT ukqqa5yguav5xrwqshq01dcpgw4 UNIQUE (anio_matricula, mes_matricula),
	CONSTRAINT eo_tbl_estudiantes_responsabilidades_penales_fk FOREIGN KEY (persona_cod) REFERENCES ocem.eo_tbl_estudiantes(persona_codigo),
	CONSTRAINT eo_tbl_estudiantes_responsabilidades_penales_fk_1 FOREIGN KEY (sistema_resp_penal_cod) REFERENCES ocem.eo_tbl_sistemas_resp_penal(codigo)
);