-- Drop table

-- DROP TABLE ocem.eo_tbl_estudiantes_tipos_capacidad;

CREATE TABLE eo_tbl_estudiantes_tipos_capacidad (
	codigo int8 NOT NULL,
	mes_matricula int4 NOT NULL,
	anio_matricula int8 NOT NULL,
	persona_cod int8 NOT NULL,
	tipo_capacidad_cod int8 NOT NULL,
	CONSTRAINT eo_tbl_estudiantes_tipos_capacidad_pkey PRIMARY KEY (codigo),
	CONSTRAINT ukjjsxasfradq96u5orle73qj57 UNIQUE (persona_cod, anio_matricula, mes_matricula),
	CONSTRAINT eo_tbl_estudiantes_tipos_capacidad_fk FOREIGN KEY (persona_cod) REFERENCES eo_tbl_estudiantes(persona_codigo),
	CONSTRAINT eo_tbl_estudiantes_tipos_capacidad_fk_1 FOREIGN KEY (tipo_capacidad_cod) REFERENCES eo_tbl_tipos_capacidad(codigo)
);