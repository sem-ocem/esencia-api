-- Drop table

-- DROP TABLE eo_tbl_estudiantes_trastornos_especificos;

CREATE TABLE eo_tbl_estudiantes_trastornos_especificos (
	codigo int8 NOT NULL,
	fe_creado timestamp NULL,
	mes_matricula int4 NOT NULL,
	anio_matricula int8 NOT NULL,
	persona_cod int8 NOT NULL,
	trastorno_especifico_cod int8 NOT NULL,
	CONSTRAINT eo_tbl_estudiantes_trastornos_especificos_pkey PRIMARY KEY (codigo),
	CONSTRAINT uk7p3rt6o70l3d8mnq4dyqgv89x UNIQUE (persona_cod, anio_matricula, mes_matricula),
	CONSTRAINT eo_tbl_estudiantes_trastornos_especificos_fk FOREIGN KEY (persona_cod) REFERENCES eo_tbl_estudiantes(persona_codigo),
	CONSTRAINT eo_tbl_estudiantes_trastornos_especificos_fk_1 FOREIGN KEY (trastorno_especifico_cod) REFERENCES eo_tbl_trastornos_especificos(codigo)
);