-- Drop table

-- DROP TABLE ocem.eo_tbl_consistencia_ie;

CREATE TABLE eo_tbl_consistencia_ie (
	codigo int8 NOT NULL,
	anio int8 NOT NULL,
	dane_anterior int8 NOT NULL,
	mes_formato_numero int4 NOT NULL,
	CONSTRAINT eo_tbl_consistencia_ie_pkey PRIMARY KEY (codigo),
	CONSTRAINT eo_tbl_consistencia_ie_fk FOREIGN KEY (dane_anterior) REFERENCES eo_tbl_ie(dane_anterior)
);
