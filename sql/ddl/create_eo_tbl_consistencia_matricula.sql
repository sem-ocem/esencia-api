-- Drop table

-- DROP TABLE ocem.eo_tbl_consistencia_matricula;

CREATE TABLE ocem.eo_tbl_consistencia_matricula (
	codigo int8 NOT NULL,
	mes_formato_numero int4 NOT NULL,
	anio int8 NOT NULL,
	persona_codigo int8 NOT NULL,
	CONSTRAINT eo_tbl_consistencia_matricula_pkey PRIMARY KEY (codigo),
	CONSTRAINT eo_tbl_consistencia_matricula_fk FOREIGN KEY (persona_codigo) REFERENCES ocem.eo_tbl_estudiantes(persona_codigo)
);

