-- Drop table

-- DROP TABLE ocem.eo_tbl_tipos_victimas_conflicto;

CREATE TABLE ocem.eo_tbl_tipos_victimas_conflicto (
	codigo int8 NOT NULL,
	descripcion varchar(100) NULL,
	nombre varchar(100) NULL,
	CONSTRAINT eo_tbl_tipos_victimas_conflicto_pkey PRIMARY KEY (codigo)
);