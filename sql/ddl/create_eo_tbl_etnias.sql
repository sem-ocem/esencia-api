-- Drop table

-- DROP TABLE ocem.eo_tbl_etnias;

CREATE TABLE ocem.eo_tbl_etnias (
	codigo int8 NOT NULL,
	descripcion varchar(100) NULL,
	nombre varchar(100) NULL,
	CONSTRAINT eo_tbl_etnias_pkey PRIMARY KEY (codigo)
);