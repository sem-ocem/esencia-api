-- Drop table

-- DROP TABLE ocem.eo_tbl_zonas;

CREATE TABLE ocem.eo_tbl_zonas (
	codigo int8 NOT NULL,
	descripcion varchar(100) NULL,
	nombre varchar(100) NULL,
	CONSTRAINT eo_tbl_zonas_pkey PRIMARY KEY (codigo)
);