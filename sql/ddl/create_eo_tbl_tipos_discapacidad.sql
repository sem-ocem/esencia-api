-- Drop table

-- DROP TABLE ocem.eo_tbl_tipos_discapacidad;

CREATE TABLE ocem.eo_tbl_tipos_discapacidad (
	codigo int8 NOT NULL,
	descripcion varchar(100) NULL,
	nombre varchar(100) NULL,
	CONSTRAINT eo_tbl_tipos_discapacidad_pkey PRIMARY KEY (codigo)
);