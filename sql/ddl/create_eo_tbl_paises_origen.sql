-- Drop table

-- DROP TABLE ocem.eo_tbl_paises_origen;

CREATE TABLE ocem.eo_tbl_paises_origen (
	codigo int8 NOT NULL,
	descripcion varchar(100) NULL,
	nombre varchar(100) NULL,
	CONSTRAINT eo_tbl_paises_origen_pkey PRIMARY KEY (codigo)
);