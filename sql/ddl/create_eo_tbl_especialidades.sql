-- Drop table

-- DROP TABLE ocem.eo_tbl_especialidades;

CREATE TABLE ocem.eo_tbl_especialidades (
	codigo int8 NOT NULL,
	descripcion varchar(100) NULL,
	nombre varchar(100) NULL,
	CONSTRAINT eo_tbl_especialidades_pkey PRIMARY KEY (codigo)
);