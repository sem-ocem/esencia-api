package co.edu.medellin.sem.ocem.esencia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import java.util.Optional;

@SpringBootApplication
public class EsenciaApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(EsenciaApiApplication.class, args);
	}

}
