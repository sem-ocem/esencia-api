package co.edu.medellin.sem.ocem.esencia.model;

import lombok.Data;

import java.util.Date;

@Data
public class EstudianteDTO {

    private Long anio;
    private Long numCod;
    private String daneCodigo;
    private Long daneAnterior;
    private String sedeConsecutivo;
    private Long documentoCod;
    private String numDocumento;
    private Long expDepartamentoCod;
    private Long expMunicipioCod;
    private String apellido1;
    private String apellido2;
    private String nombre1;
    private String nombre2;
    private String direccionResidencia;
    private String telefono;
    private Long resDepartamentoCod;
    private Long resMunicipioCod;
    private Long estrato;
    private String sisben;
    private Date fechaNacimiento;
    private Long nacDepartamentoCod;
    private Long nacMunicipioCod;
    private String genero;
    private Long pobVictConfCod;
    private Long expulDepartamentoCod;
    private Long expulMunicipioCod;
    private String provieneSectorPriv;
    private String provieneOtrMun;
    private Long discapacidadCod;
    private Long capacidadCod;
    private Long etniaCod;
    private Long resguardoCod;
    private String insFamiliar;
    private Long jornadaCod;
    private Long caracterCod;
    private Long especialidadCod;
    private Long gradoCod;
    private String grupo;
    private Long metodologiaCod;
    private String matriculaContratada;
    private String repitente;
    private String nuevo;
    private Long sitAcadAnioAnteriorCod;
    private Long condAcadAnioAnteriorCod;
    private Long fuenteRecursosCod;
    private Long zonaCod;
    private String madreCabezaFamilia;
    private String hijoMadreCabezaFamilia;
    private String veterano;
    private String heroe;
    private Long internadoCod;
    private String codigoValoracion1;
    private String codigoValoracion2;
    private String numConvenio;
    private Long personaCodigo;
    private Long apoyoAcademicoEspecialCod;
    private Long sistemaRespPenalCod;
    private Long paisOrigenCod;
    private Long trastornosEspecificosCod;

}
