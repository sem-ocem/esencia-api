package co.edu.medellin.sem.ocem.esencia.mapper;

import co.edu.medellin.sem.ocem.esencia.mapper.support.MapperDTO;
import co.edu.medellin.sem.ocem.esencia.mapper.support.MapperDTOConfig;
import co.edu.medellin.sem.ocem.esencia.model.EstudianteDTO;
import co.edu.medellin.sem.ocem.esencia.repository.entity.Estudiante;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", config = MapperDTOConfig.class)
public abstract class EstudianteDTOMapper implements MapperDTO<EstudianteDTO, Estudiante> {

    /**
     * Este metodo abstracto es el encargado retonar el objeto Estudiante dto por medio del parametro de entrada
     * Estudiante entity
     * @param entity
     * @return EstudianteDTO
     */
    @Override
    public abstract EstudianteDTO toDTO(Estudiante entity);

    /**
     * Este metodo abstracto es el encargado retonar el objeto Estudiante entity por medio del parametro de entrada
     * Estudiante dto
     * @param dto
     * @return Estudiante
     */
    @Override
    public abstract Estudiante toEntity(EstudianteDTO dto);
}
