package co.edu.medellin.sem.ocem.esencia.mapper.support;

public interface MapperDTO<D, E> {

    /**
     * Este metdodo es el encargado de mappear desde los objetos de datos del modulo repository al modulo
     * dto, donde el parametro de entrada es de objeto entity y el de retorno es dto
     * @param entity
     * @return D
     */
    D toDTO(E entity);

    /**
     * Este metdodo es el encargado de mappear desde los objetos de datos del modulo dto al modulo
     * repositorio, donde el parametro de entrada es de objeto dto y el de retorno es entity
     * @param dto
     * @return E
     */
    E toEntity(D dto);
}
