package co.edu.medellin.sem.ocem.esencia.model;

import lombok.Data;

/**
 * Esta clase es para obtener la entidad IE que son las instituciones de Medellin
 *
 * @author israel.villegas@medellin.gov.co
 */
@Data
public class IEDTO {


    /**
     * Atributo que obtiene o almacena el codigo de la division politica administrativa
     */
    private Long zonaDivisionPoliticaAdmin;

    /**
     * Atributo que obtiene o almacena el nombre de la zona de la division politica administrativa
     */
    private String nombreZonaDivisionPoliticaAdmin;

    /**
     * Atributo que obtiene o almacena el codigo del nit
     */
    private String nit;

    /**
     * Atributo que obtiene o almacena el codigo Dane que identifica la Institucion
     */
    private Long daneAnterior;

    /**
     * Atributo que obtiene o almacena el codigo Dane de establecimiento que pertenece la IE; unas IEs son identificadas
     * como establecimiento.
     */
    private Long daneEstablecimiento;

    /**
     * Atributo que obtiene o almacena el codigo el consecutivo Dane
     */
    private Long daneConsecutivo;

    /**
     * Atributo que obtiene o almacena el nombre de la sede
     */
    private String nombreSede;

    /**
     * Atributo que obtiene o almacena el nombre de tipo de sede
     */
    private String tipoSede;

    /**
     * Atributo que obtiene o almacena el codigo de la comuna
     */
    private Long comunaCod;

    /**
     * Atributo que obtiene o almacena el nombre de la columna
     */
    private String nombreComuna;

    /**
     * Atributo que obtiene o almacena el codigo del nucleo
     */
    private Integer nucleo;

    /**
     * Atributo que obtiene o almacena el numero oficial
     */
    private Integer numeroOficial;

    /**
     * Atributo que obtiene o almacena el nombre de servicio que ofrece la IE
     */
    private String prestacionServicio;

    /**
     * Atributo que obtiene o almacena el sector de la IE
     */
    private String sector;

    /**
     * Atributo que obtiene o almacena la zona sineb
     */
    private String zonaSineb;

    /**
     * Atributo que obtiene o almacena la zona georeferenciado
     */
    private String zonaGeoreferenciado;

    /**
     * Atributo que obtiene o almacena el Barrio
     */
    private String barrio;

    /**
     * Atributo que obtiene o almacena la vereda
     */
    private String vereda;

    /**
     * Atributo que obtiene o almacena la direccion de la IE
     */
    private String direccion;

    /**
     * Atributo que obtiene o almacena el telefono de la IE
     */
    private String telefono;

    /**
     * Atributo que obtiene o almacena el correo de la IE
     */
    private String correoElectronico;

    /**
     * Atributo que obtiene o almacena el correo de la IE
     */
    private String rector;

    /**
     * Atributo que obtiene o almacena la identificacion del rector
     */
    private Long rectorCedula;

    /**
     * Atributo que obtiene o almacena las observaciones de la IE
     */
    private String observaciones;

    /**
     * Atributo que obtiene o almacena los celulares
     */
    private String celulares;

}

