package co.edu.medellin.sem.ocem.esencia.batch.estudiante.job;

import org.springframework.batch.core.step.skip.SkipLimitExceededException;
import org.springframework.batch.core.step.skip.SkipPolicy;

public class JobSkipPolicy implements SkipPolicy {

    @Override
    public boolean shouldSkip(Throwable t, int failedCount) throws SkipLimitExceededException {
        return true;
    }
}
