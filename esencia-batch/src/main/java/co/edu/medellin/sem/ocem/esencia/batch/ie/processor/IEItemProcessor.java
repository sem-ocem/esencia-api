package co.edu.medellin.sem.ocem.esencia.batch.ie.processor;

import co.edu.medellin.sem.ocem.esencia.model.IEDTO;
import co.edu.medellin.sem.ocem.esencia.repository.entity.Comuna;
import co.edu.medellin.sem.ocem.esencia.repository.entity.ConsistenciaIE;
import co.edu.medellin.sem.ocem.esencia.repository.entity.IE;
import co.edu.medellin.sem.ocem.esencia.repository.port.ComunaRepository;
import co.edu.medellin.sem.ocem.esencia.repository.port.ZonaDivisionPoliticaAdminRepository;
import co.edu.medellin.sem.ocem.esencia.util.date.DateUtil;
import co.edu.medellin.sem.ocem.esencia.util.exception.CustomException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Esta clase es para hacer el proceso de mapeo entre el archivo xlsx y el objerto IE
 *
 * @author israel.villegas@medellin.gov.co
 */
@Slf4j
@Component
public class IEItemProcessor implements ItemProcessor<IEDTO, IE> {

    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private ZonaDivisionPoliticaAdminRepository zonaDivisionPoliticaAdminRepository;


    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private ComunaRepository comunaRepository;

    /**
     * Metodo que se encarga de hacer el mapeo del los datos que vienen del reader al witter
     *
     * @param iEDTO es el objeto que llega para ser mapeado
     * @return Anexo es el objeto al que se debe convertir
     * @throws Exception
     */
    @Override
    public IE process(IEDTO iEDTO) throws Exception {

        IE ie = new IE();

        ie.setConsistenciaIE(buildConsistenciaMatricula(iEDTO));
        ie.setZonaDivisionPoliticaAdminCod(iEDTO.getZonaDivisionPoliticaAdmin());
        if(!zonaDivisionPoliticaAdminRepository.existsById(iEDTO.getZonaDivisionPoliticaAdmin())){
            log.error("Lanzar una excepcion1");
            log.error("El codigo " + iEDTO.getZonaDivisionPoliticaAdmin() + " no existe en la tabla eo_tbl_comuna, revisar al tabla eo_tbl_errores_registros");
            throw new CustomException("El codigo " + iEDTO.getZonaDivisionPoliticaAdmin() + " no existe en la tabla oe_tbl_zonas_divisiones_politicas_admin", "ZONA  (División Político Adminisrativa)");
        }
        ie.setNit(iEDTO.getNit());
        ie.setDaneAnterior(iEDTO.getDaneAnterior());
        ie.setDaneEstablecimiento(iEDTO.getDaneEstablecimiento());
        ie.setDaneConsecutivo(iEDTO.getDaneConsecutivo());
        if(ie.getDaneAnterior() != null && ie.getSector() != null) {
             ie.setDaneAnteriorSector(iEDTO.getDaneAnterior().toString().concat(iEDTO.getSector()));
        }
        ie.setNombreSede(iEDTO.getNombreSede());
        ie.setTipoSede(iEDTO.getTipoSede());
        ie.setComunaCod(iEDTO.getComunaCod());
        if(!comunaRepository.existsById(iEDTO.getComunaCod())){
            log.error("El codigo " + iEDTO.getComunaCod() + " no existe en la tabla eo_tbl_comuna, revisar al tabla eo_tbl_errores_registros");
            throw new CustomException("El codigo " + iEDTO.getComunaCod() + " no existe en la tabla eo_tbl_comuna", "COMUNA");
        }

        ie.setNucleo(iEDTO.getNucleo());
        ie.setNumeroOficial(iEDTO.getNumeroOficial());
        ie.setPrestacionServicio(iEDTO.getPrestacionServicio());
        ie.setSector(iEDTO.getSector());
        ie.setZonaSineb(iEDTO.getZonaSineb());
        ie.setZonaGeoreferenciado(iEDTO.getZonaGeoreferenciado());
        ie.setBarrio(iEDTO.getBarrio());
        ie.setVereda(iEDTO.getVereda());
        ie.setDireccion(iEDTO.getDireccion());
        ie.setTelefono(iEDTO.getTelefono());
        ie.setCorreoElectronico(iEDTO.getCorreoElectronico());
        ie.setRector(iEDTO.getRector());
        ie.setRectorCedula(iEDTO.getRectorCedula());
        ie.setObservaciones(iEDTO.getObservaciones());
        ie.setCelulares(iEDTO.getCelulares());

        return ie;
    }

    /**
     * Metodo que se encarga de construir el objeto ConsistenciaIE
     * @param ieDTO
     * @return ConsistenciaIE
     */
    private ConsistenciaIE buildConsistenciaMatricula(IEDTO ieDTO) {
        ConsistenciaIE consistenciaIE = new ConsistenciaIE();
        consistenciaIE.setCodigo(Long.valueOf(String.valueOf(ieDTO.getDaneAnterior())+String.valueOf(DateUtil.getYear())+String.valueOf(DateUtil.getMonthNumber())));
        consistenciaIE.setAnio((long) DateUtil.getYear());
        consistenciaIE.setMesFormatoNumero(DateUtil.getMonthNumber());
        consistenciaIE.setDaneAnterior(ieDTO.getDaneAnterior());

        return consistenciaIE;
    }
}
