package co.edu.medellin.sem.ocem.esencia.batch.estudiante.listener;

import co.edu.medellin.sem.ocem.esencia.model.EstudianteDTO;
import co.edu.medellin.sem.ocem.esencia.repository.entity.ErrorRegistro;
import co.edu.medellin.sem.ocem.esencia.repository.entity.Estudiante;
import co.edu.medellin.sem.ocem.esencia.repository.port.ErrorRegistroRepository;
import co.edu.medellin.sem.ocem.esencia.util.exception.CustomException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.*;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class EstudianteSkipListener implements SkipListener<EstudianteDTO, Estudiante>, StepExecutionListener {


    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private ErrorRegistroRepository errorRegistroRepository;

    JobParameters jobParameters;

    @Override
    public void beforeStep(final StepExecution stepExecution) {
        jobParameters = stepExecution.getJobParameters();
    }


    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        return null;
    }


    @Override
    public void onSkipInRead(Throwable t) {
        System.out.println("StepSkipListener - onSkipInRead");
    }

    @Override
    public void onSkipInWrite(Estudiante item, Throwable t) {
        ErrorRegistro errorRegistro = new ErrorRegistro();
        errorRegistro.setNombreArchivo(jobParameters.getParameters().get("fileName").toString());
        errorRegistro.setRegistroCod(item.getPersonaCodigo());
        errorRegistro.setTrazaError(t.getMessage());
        //errorRegistro.setNombreVariableArchivo("MUN_EXP");
        errorRegistroRepository.save(errorRegistro);
    }

    @Override
    public void onSkipInProcess(EstudianteDTO item, Throwable t) {
        ErrorRegistro errorRegistro = new ErrorRegistro();
        errorRegistro.setNombreArchivo(jobParameters.getParameters().get("fileName").toString());
        errorRegistro.setRegistroCod(item.getPersonaCodigo());
        errorRegistro.setTrazaError(t.toString());
        errorRegistro.setDescripcion(t.getMessage());
        if(t instanceof CustomException){
            errorRegistro.setNombreVariableArchivo(((CustomException) t).getVariableFile());
        }
        errorRegistroRepository.save(errorRegistro);
    }
}