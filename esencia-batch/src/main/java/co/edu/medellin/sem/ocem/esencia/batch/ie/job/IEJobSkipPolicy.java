package co.edu.medellin.sem.ocem.esencia.batch.ie.job;

import org.springframework.batch.core.step.skip.SkipLimitExceededException;
import org.springframework.batch.core.step.skip.SkipPolicy;

/**
 * Esta clase es la encargada de saltarse los registros que tienen errores
 */
public class IEJobSkipPolicy implements SkipPolicy {

    @Override
    public boolean shouldSkip(Throwable t, int failedCount) throws SkipLimitExceededException {
        return true;
    }
}
