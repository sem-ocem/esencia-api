package co.edu.medellin.sem.ocem.esencia.batch.ie;

import co.edu.medellin.sem.ocem.esencia.model.IEDTO;
import co.edu.medellin.sem.ocem.esencia.util.exception.CustomException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.excel.ExcelFileParseException;
import org.springframework.batch.item.excel.RowMapper;
import org.springframework.batch.item.excel.support.rowset.RowSet;

/**
 * Esta clase implementa la interfaz RowMapper para la transformacion de los registros de excel a el objeto IEDTO
 *
 * @author israel.villegas@medellin.gov.co
 */
@Slf4j
public class IEExcelRowMapper implements RowMapper<IEDTO> {


    /**
     * Metodo que se encarga de obtener cada resgistro de excel que esta en la variable rs y lo convierte al objeto
     * IEDTO
     *
     * @param rs
     * @return IEDTO
     * @throws Exception
     */
    @Override
    public IEDTO mapRow(RowSet rs) throws CustomException {
        IEDTO iedto = new IEDTO();

        if (rs == null || rs.getCurrentRow() == null) {
            return null;
        }
        try {
            String zonaTmp = rs.getColumnValue(0);
            Long zona = Long.parseLong(zonaTmp.replace("\n", ""));
            iedto.setZonaDivisionPoliticaAdmin(zona);

            iedto.setNombreZonaDivisionPoliticaAdmin(rs.getColumnValue(1));
            iedto.setNit(rs.getColumnValue(2));

            String daneAnteriorTmp = rs.getColumnValue(3);
            Long daneAnterior = Long.parseLong(daneAnteriorTmp.replace("\n", ""));
            iedto.setDaneAnterior(daneAnterior);

            String daneEstablecimientoTmp = rs.getColumnValue(4);
            Long daneEstablecimiento = Long.parseLong(daneEstablecimientoTmp.replace("\n", ""));
            iedto.setDaneEstablecimiento(daneEstablecimiento);

            String daneConsecutivoTmp = rs.getColumnValue(5);
            Long daneConsecutivo = Long.parseLong(daneConsecutivoTmp.replace("\n", ""));
            iedto.setDaneConsecutivo(daneConsecutivo);

            iedto.setNombreSede(rs.getColumnValue(6));
            iedto.setTipoSede(rs.getColumnValue(7));

            String comunaTmp = rs.getColumnValue(8);
            Long comuna = Long.parseLong(comunaTmp.replace("\n", ""));
            iedto.setComunaCod(comuna);

            //iedto.setNombreComuna(rs.getColumnValue(9));

            String nucleoTmp = rs.getColumnValue(10);
            int nucleo = Integer.parseInt(nucleoTmp.replace("\n", ""));
            iedto.setNucleo(nucleo);

            String numeroOficialTmp = rs.getColumnValue(11);
            int numeroOficial = Integer.parseInt(numeroOficialTmp.replace("\n", ""));
            iedto.setNumeroOficial(numeroOficial);

            iedto.setPrestacionServicio(rs.getColumnValue(12));
            iedto.setSector(rs.getColumnValue(13));
            iedto.setZonaSineb(rs.getColumnValue(14));
            iedto.setZonaGeoreferenciado(rs.getColumnValue(15));
            iedto.setBarrio(rs.getColumnValue(16));
            iedto.setVereda(rs.getColumnValue(17));
            iedto.setDireccion(rs.getColumnValue(18));
            iedto.setTelefono(rs.getColumnValue(19));
            iedto.setCorreoElectronico(rs.getColumnValue(20));
            iedto.setRector(rs.getColumnValue(21));
            String rectorCedulaTmp = rs.getColumnValue(22);
            Long rectorCedula = Long.parseLong(rectorCedulaTmp.replace("\n", "").trim());
            iedto.setRectorCedula(rectorCedula);
            iedto.setObservaciones(rs.getColumnValue(23));
            iedto.setCelulares(rs.getColumnValue(24));

        }catch (NumberFormatException nfe){
            throw new CustomException("Esto es lo que causa el problema. "+nfe.getMessage(), nfe);
        } catch ( IllegalArgumentException ie) {
            throw new CustomException("Tipo de dato erroneo", ie);
        }catch (ExcelFileParseException ee) {
            throw new CustomException("Tipo de dato erroneo", ee);
        }

        return iedto;
    }
}
