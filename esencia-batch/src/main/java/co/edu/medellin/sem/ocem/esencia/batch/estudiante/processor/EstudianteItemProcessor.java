package co.edu.medellin.sem.ocem.esencia.batch.estudiante.processor;

import co.edu.medellin.sem.ocem.esencia.model.EstudianteDTO;
import co.edu.medellin.sem.ocem.esencia.repository.entity.*;
import co.edu.medellin.sem.ocem.esencia.repository.port.*;
import co.edu.medellin.sem.ocem.esencia.util.Constant;
import co.edu.medellin.sem.ocem.esencia.util.date.DateUtil;
import co.edu.medellin.sem.ocem.esencia.util.exception.CustomException;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * Esta clase es para hacer el proceso de mapeo entre el archivo txt y el objerto Estudiante
 *
 * @author israel.villegas@medellin.gov.co
 */

@Component
public class EstudianteItemProcessor implements ItemProcessor<EstudianteDTO, Estudiante> {

    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private MunicipioRepository municipioRepository;

    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private EstudianteResidenciaRepository estudianteResidenciaRepository;

    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private EstudianteExpedicionDocumentoRepository estudianteExpedicionDocumentoRepository;

    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private EstudianteFuenteRecursosRepository estudianteFuenteRecursosRepository;

    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private FuenteRecursoRepository fuenteRecursoRepository;

    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private EstudianteRespPenalesRepository estudianteRespPenalesRepository;

    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private EstudianteTrastornoEspecificoRepository estudianteTrastornoEspecificoRepository;

    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private EstudianteTipoCapacidadRepository estudianteTipoCapacidadRepository;

    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private EstudianteEspecialidadRepository estudianteEspecialidadRepository;

    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private EstudianteSituacionAcademicaAnioAnteriorRepository situacionAcademicaAnioAnteriorRepository;

    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private EstudianteCondicionAcademicaAnioAnteriorRepository estudianteCondicionAcademicaAnioAnteriorRepository;

    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private EstudianteCaracterRepository estudianteCaracterRepository;

    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private EstudianteGradoRepository estudianteGradoRepository;

    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private GradoMetodologiaRepository gradoMetodologiaRepository;

    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private EtniaRepository etniaRepository;

    JobParameters jobParameters;

    @BeforeStep
    public void beforeStep(final StepExecution stepExecution) {
        jobParameters = stepExecution.getJobParameters();
    }

    /**
     * Metodo que se encarga de hacer el mapeo del los datos que vienen del reader al witter
     *
     * @param estudianteDTO es el objeto que llega para ser mapeado
     * @return Anexo es el objeto al que se debe convertir
     * @throws Exception
     */
    @Override
    public Estudiante process(EstudianteDTO estudianteDTO) throws CustomException {


        Estudiante estudiante = new Estudiante();
        if (estudianteDTO.getPobVictConfCod() != null && estudianteDTO.getPobVictConfCod() < Constant.CODIGO_NO_APLICA_TIPO_VICTIMA_CONFLICTO) {
            estudiante.setEstudianteVictimaConflicto(buildEstudianteVictimaConflicto(estudianteDTO));
        }

        if (estudianteDTO.getApoyoAcademicoEspecialCod() != null && estudianteDTO.getApoyoAcademicoEspecialCod() > Constant.CODIGO_NO_APLICA_APOYO_ACADEMICO_ESPECIAL) {
            estudiante.setEstudianteApoyoAcademicoEspecial(buildEstudianteApoyoAcademicoEspecial(estudianteDTO));
        }

        if (estudianteDTO.getCapacidadCod() != null && estudianteDTO.getCapacidadCod() != Constant.CODIGO_NO_APLICA_TIPO_CAPACIDAD) {
            List<EstudianteTipoCapacidad> estudianteTipoCapacidadList = estudianteTipoCapacidadRepository.findByPersonaCod(estudianteDTO.getPersonaCodigo());
            if (estudianteTipoCapacidadList == null || estudianteTipoCapacidadList.isEmpty()) {
                estudiante.setEstudianteTipoCapacidad(buildEstudianteTipoCapacidad(estudianteDTO));
            } else {
                if (!estudianteTipoCapacidadList.get(0).getTipoCapacidad().equals(estudianteDTO.getCapacidadCod())) {
                    estudiante.setEstudianteTipoCapacidad(buildEstudianteTipoCapacidad(estudianteDTO));
                }
            }
        }
        estudiante.setPersonaCodigo(estudianteDTO.getPersonaCodigo());
        estudiante.setMes(DateUtil.getMonth());
        estudiante.setConsistenciaMatricula(buildConsistenciaMatricula(estudianteDTO));
        estudiante.setDaneAnterior(estudianteDTO.getDaneAnterior());
        estudiante.setDaneCodigo(estudianteDTO.getDaneCodigo());
        estudiante.setSedeConsecutivo(estudianteDTO.getSedeConsecutivo());
        estudiante.setNumCod(estudianteDTO.getNumCod());
        estudiante.setTipoDocumentoCod(estudianteDTO.getDocumentoCod());
        estudiante.setNumDocumento(estudianteDTO.getNumDocumento());
        estudiante.setExpDepartamentoCod(estudianteDTO.getExpDepartamentoCod());
        estudiante.setExpMunicipioCod(estudianteDTO.getExpMunicipioCod());
        estudiante.setApellido1(estudianteDTO.getApellido1());
        estudiante.setApellido2(estudianteDTO.getApellido2());
        estudiante.setNombre1(estudianteDTO.getNombre1());
        estudiante.setNombre2(estudianteDTO.getNombre2());
        estudiante.setTelefono(estudianteDTO.getTelefono());

        if (estudianteDTO.getResMunicipioCod() != null && estudianteDTO.getResMunicipioCod() > 0) {
            Municipio municipioResp = findMunicipio(estudianteDTO.getResMunicipioCod(), estudianteDTO.getResDepartamentoCod());

            List<EstudianteResidencia> estudianteResidenciaRespList = estudianteResidenciaRepository.findByPersonaCodAndMunicipioCodList(estudianteDTO.getPersonaCodigo(), municipioResp.getCodigo());
            if (estudianteResidenciaRespList == null || estudianteResidenciaRespList.isEmpty()) {
                estudiante.setEstudianteResidencia(buildEstudianteResidencia(estudianteDTO, municipioResp.getCodigo()));
            } else {
                if (!estudianteResidenciaRespList.get(0).getDireccion().equals(estudianteDTO.getDireccionResidencia())) {
                    estudiante.setEstudianteResidencia(buildEstudianteResidencia(estudianteDTO, municipioResp.getCodigo()));
                }
            }
        }
        estudiante.setEstrato(estudianteDTO.getEstrato());
        estudiante.setSisben(estudianteDTO.getSisben());
        estudiante.setFechaNacimiento(estudianteDTO.getFechaNacimiento());
        estudiante.setNacDepartamentoCod(estudianteDTO.getNacDepartamentoCod());
        estudiante.setNacMunicipioCod(estudianteDTO.getNacMunicipioCod());
        estudiante.setGenero(estudianteDTO.getGenero());
        estudiante.setPobVictConfCod(estudianteDTO.getPobVictConfCod());

        if (estudianteDTO.getExpDepartamentoCod() != null && estudianteDTO.getExpDepartamentoCod() > 0) {
            Municipio municipioResp = findMunicipio(estudianteDTO.getExpMunicipioCod(), estudianteDTO.getExpDepartamentoCod());

            EstudianteExpedicionDocumento estudianteExpedicionDocumentoResp = estudianteExpedicionDocumentoRepository.findByPersonaCodAndMunicipioCod(estudianteDTO.getPersonaCodigo(), municipioResp.getCodigo());
            if (estudianteExpedicionDocumentoResp == null) {
                estudiante.setEstudianteExpedicionDocumento(buildEstudianteExpDocumento(estudianteDTO, municipioResp.getCodigo()));
            }
        }
        estudiante.setProvieneSectorPriv(estudianteDTO.getProvieneSectorPriv());
        estudiante.setProvieneOtrMun(estudianteDTO.getProvieneOtrMun());
        estudiante.setDiscapacidadCod(estudianteDTO.getDiscapacidadCod());
        estudiante.setCapacidadCod(estudianteDTO.getCapacidadCod());
        estudiante.setEtniaCod(estudianteDTO.getEtniaCod());

        if (!etniaRepository.existsById(estudianteDTO.getEtniaCod())) {
            throw new CustomException("El codigo " + estudianteDTO.getEtniaCod() + " no existe en la tabla eo_tbl_etnia", "ETNIA");
        }

        estudiante.setResguardoCod(estudianteDTO.getResguardoCod());
        if (!etniaRepository.existsById(estudianteDTO.getResguardoCod())) {
            throw new CustomException("El codigo " + estudianteDTO.getResguardoCod() + " no existe en la tabla eo_tbl_resguardo", "RES");
        }
        estudiante.setInsFamiliar(estudianteDTO.getInsFamiliar());
        estudiante.setJornadaCod(estudianteDTO.getJornadaCod());
        estudiante.setCaracterCod(estudianteDTO.getCaracterCod());
        if (estudianteDTO.getCaracterCod() != null && estudianteDTO.getCaracterCod() != Constant.CODIGO_NO_APLICA_CARACTER) {
            List<EstudianteCaracter> estudianteCaracterList = estudianteCaracterRepository.findByPersonaCod(estudianteDTO.getPersonaCodigo());
            if (estudianteCaracterList == null || estudianteCaracterList.isEmpty()) {
                estudiante.setEstudianteCaracter(buildEstudianteCaracter(estudianteDTO));
            } else {
                if (!estudianteCaracterList.get(0).getCaracterCod().equals(estudianteDTO.getCaracterCod())) {
                    estudiante.setEstudianteCaracter(buildEstudianteCaracter(estudianteDTO));
                }
            }
        }
        if (estudianteDTO.getEspecialidadCod() != null && estudianteDTO.getEspecialidadCod() != Constant.CODIGO_NO_APLICA_ESPECIALIDADES) {
            List<EstudianteEspecialidad> estudianteEspecialidadList = estudianteEspecialidadRepository.findByPersonaCod(estudianteDTO.getPersonaCodigo());
            if (estudianteEspecialidadList == null || estudianteEspecialidadList.isEmpty()) {
                estudiante.setEstudianteEspecialidad(buildEstudianteEspecialidad(estudianteDTO));
            } else {
                if (!estudianteEspecialidadList.get(0).getEspecialidadesCod().equals(estudianteDTO.getEspecialidadCod())) {
                    estudiante.setEstudianteEspecialidad(buildEstudianteEspecialidad(estudianteDTO));
                }
            }
        }
        estudiante.setGradoCod(estudianteDTO.getGradoCod());
        if (estudianteDTO.getGradoCod() != null) {
            List<EstudianteGrado> estudianteGradoList = estudianteGradoRepository.findByPersonaCod(estudianteDTO.getPersonaCodigo());
            if (estudianteGradoList == null || estudianteGradoList.isEmpty()) {
                estudiante.setEstudianteGrado(buildEstudianteGrado(estudianteDTO));
            } else {
                if (!estudianteGradoList.get(0).getGradoCod().equals(estudianteDTO.getGradoCod())) {
                    estudiante.setEstudianteGrado(buildEstudianteGrado(estudianteDTO));
                }
            }
        }
        estudiante.setGrupo(estudianteDTO.getGrupo());
        estudiante.setMetodologiaCod(estudianteDTO.getMetodologiaCod());
        if (estudianteDTO.getMetodologiaCod() != null) {
            List<GradoMetodologia> gradoMetodologiaList = gradoMetodologiaRepository.findByPersonaCod(estudianteDTO.getPersonaCodigo());
            if (gradoMetodologiaList == null || gradoMetodologiaList.isEmpty()) {
                estudiante.setGradoMetodologia(buildGradoMetodologia(estudianteDTO));
            } else {
                if (!gradoMetodologiaList.get(0).getMetodologiaCod().equals(estudianteDTO.getMetodologiaCod())) {
                    estudiante.setGradoMetodologia(buildGradoMetodologia(estudianteDTO));
                }
            }
        }
        estudiante.setMatriculaContratada(estudianteDTO.getMatriculaContratada());
        estudiante.setSector(findSector(estudianteDTO.getMatriculaContratada()));

        estudiante.setRepitente(estudianteDTO.getRepitente());
        estudiante.setNuevo(estudianteDTO.getNuevo());
        estudiante.setSitAcadAnioAnteriorCod(estudianteDTO.getSitAcadAnioAnteriorCod());

        if (estudianteDTO.getSitAcadAnioAnteriorCod() != null) {
            List<EstudianteSituacionAcademicaAnioAnterior> estudianteSituacionAcademicaAnioAnteriorList = situacionAcademicaAnioAnteriorRepository.findByPersonaCod(estudianteDTO.getPersonaCodigo());
            if (estudianteSituacionAcademicaAnioAnteriorList == null || estudianteSituacionAcademicaAnioAnteriorList.isEmpty()) {
                estudiante.setEstudianteSituacionAcademicaAnioAnterior(buildEstudianteSitAcadAnioAnterior(estudianteDTO));
            } else {
                if (estudianteSituacionAcademicaAnioAnteriorList.get(0).getAnioMatricula() < estudianteDTO.getAnio()) {
                    estudiante.setEstudianteSituacionAcademicaAnioAnterior(buildEstudianteSitAcadAnioAnterior(estudianteDTO));
                }
            }
        }
        if (estudianteDTO.getCondAcadAnioAnteriorCod() != null && estudianteDTO.getCondAcadAnioAnteriorCod() != Constant.CODIGO_NO_APLICA_COND_ACAD_ANIOS_ANTERIORES) {
            List<EstudianteCondicionAcademicaAnioAnterior> condicionAcademicaAnioAnteriorList = estudianteCondicionAcademicaAnioAnteriorRepository.findByPersonaCod(estudianteDTO.getPersonaCodigo());
            if (condicionAcademicaAnioAnteriorList == null || condicionAcademicaAnioAnteriorList.isEmpty()) {
                estudiante.setEstudianteCondicionAcademicaAnioAnterior(buildEstudianteCondAcadAnioAnterior(estudianteDTO));
            } else {
                if (condicionAcademicaAnioAnteriorList.get(0).getAnioMatricula() < estudianteDTO.getAnio()) {
                    estudiante.setEstudianteCondicionAcademicaAnioAnterior(buildEstudianteCondAcadAnioAnterior(estudianteDTO));
                }
            }
        }
        FuenteRecurso fuenteRecurso = fuenteRecursoRepository.findByfuenteRecursosEstandarCodAndPara(estudianteDTO.getFuenteRecursosCod(), Constant.ESTUDIANTE).orElse(new FuenteRecurso());
        estudiante.setFuenteRecursosCod(fuenteRecurso.getCodigo());
        if (fuenteRecurso != null && fuenteRecurso.getCodigo() != null) {
            List<EstudianteFuenteRecurso> estudianteFuenteRecursoRespList = estudianteFuenteRecursosRepository.findByPersonaCod(estudianteDTO.getPersonaCodigo());
            if (estudianteFuenteRecursoRespList == null || estudianteFuenteRecursoRespList.isEmpty()) {
                estudiante.setEstudianteFuenteRecurso(buildEstudianteFuenteRecurso(estudianteDTO, fuenteRecurso.getCodigo()));
            } else {
                if (!estudianteFuenteRecursoRespList.get(0).getFuenteRecursosCod().equals(fuenteRecurso.getCodigo())) {
                    estudiante.setEstudianteFuenteRecurso(buildEstudianteFuenteRecurso(estudianteDTO, fuenteRecurso.getCodigo()));
                }
            }
        }
        if (estudianteDTO.getZonaCod() != null && estudianteDTO.getZonaCod() != Constant.CODIGO_NO_APLICA_ZONA) {
            estudiante.setEstudianteZona(buildEstudianteZona(estudianteDTO));
        }
        estudiante.setMadreCabezaFamilia(estudianteDTO.getMadreCabezaFamilia());
        estudiante.setHijoMadreCabezaFamilia(estudianteDTO.getHijoMadreCabezaFamilia());
        estudiante.setVeterano(estudianteDTO.getVeterano());
        estudiante.setHeroe(estudianteDTO.getHeroe());
        estudiante.setInternadoCod(estudianteDTO.getInternadoCod());
        estudiante.setCodigoValoracion1(estudianteDTO.getCodigoValoracion1());
        estudiante.setCodigoValoracion2(estudianteDTO.getCodigoValoracion2());
        estudiante.setNumConvenio(estudianteDTO.getNumConvenio());
        estudiante.setApoyoAcademicoEspecialCod(estudianteDTO.getApoyoAcademicoEspecialCod());
        if (estudianteDTO.getSistemaRespPenalCod() != null && estudianteDTO.getSistemaRespPenalCod() != Constant.CODIGO_NO_APLICA_SISTEMA_RESP_PENAL) {
            List<EstudianteRespPenal> estudianteRespPenalList = estudianteRespPenalesRepository.findByPersonaCod(estudianteDTO.getPersonaCodigo());
            if (estudianteRespPenalList == null || estudianteRespPenalList.isEmpty()) {
                estudiante.setEstudianteRespPenal(buildEstudianteRespPenal(estudianteDTO));
            } else {
                if (!estudianteRespPenalList.get(0).getSistemaRespPenalCod().equals(estudianteDTO.getSistemaRespPenalCod())) {
                    estudiante.setEstudianteRespPenal(buildEstudianteRespPenal(estudianteDTO));
                }
            }
        }
        estudiante.setPaisOrigenCod(estudianteDTO.getPaisOrigenCod());
        if (estudianteDTO.getTrastornosEspecificosCod() != null && estudianteDTO.getTrastornosEspecificosCod() != Constant.CODIGO_NO_APLICA_TRASTORNOS_ESPECIFICOS) {
            List<EstudianteTrastornoEspecifico> estudianteTrastornoEspecificoList = estudianteTrastornoEspecificoRepository.findByPersonaCod(estudianteDTO.getPersonaCodigo());
            if (estudianteTrastornoEspecificoList == null || estudianteTrastornoEspecificoList.isEmpty()) {
                estudiante.setEstudianteTrastornoEspecifico(buildEstudianteTrastornoEspecifico(estudianteDTO));
            } else {
                if (!estudianteTrastornoEspecificoList.get(0).getTrastornoEspecificoCod().equals(estudianteDTO.getTrastornosEspecificosCod())) {
                    estudiante.setEstudianteTrastornoEspecifico(buildEstudianteTrastornoEspecifico(estudianteDTO));
                }
            }
        }

        return estudiante;
    }


    private EstudianteApoyoAcademicoEspecial buildEstudianteApoyoAcademicoEspecial(EstudianteDTO estudianteDTO) {
        EstudianteApoyoAcademicoEspecial estudianteApoyoAcademicoEspecial = new EstudianteApoyoAcademicoEspecial();
        estudianteApoyoAcademicoEspecial.setPersonaCod(estudianteDTO.getPersonaCodigo());
        estudianteApoyoAcademicoEspecial.setApoyoAcadEspCod(estudianteDTO.getApoyoAcademicoEspecialCod());
        estudianteApoyoAcademicoEspecial.setAnioMatricula(estudianteDTO.getAnio());
        estudianteApoyoAcademicoEspecial.setMesMatricula(DateUtil.getMonthNumber());

        return estudianteApoyoAcademicoEspecial;
    }

    private EstudianteVictimaConflicto buildEstudianteVictimaConflicto(EstudianteDTO estudianteDTO) {
        EstudianteVictimaConflicto estudianteVictimaConflicto = new EstudianteVictimaConflicto();
        estudianteVictimaConflicto.setPersonaCod(estudianteDTO.getPersonaCodigo());
        estudianteVictimaConflicto.setVictimaConflictoCod(estudianteDTO.getPobVictConfCod());
        estudianteVictimaConflicto.setAnioMatricula(estudianteDTO.getAnio());
        estudianteVictimaConflicto.setMesMatricula(DateUtil.getMonthNumber());
        Municipio municipioRes = findMunicipio(estudianteDTO.getExpulMunicipioCod(), estudianteDTO.getExpulDepartamentoCod());
        estudianteVictimaConflicto.setMunicipioCod(municipioRes.getCodigo());


        return estudianteVictimaConflicto;
    }

    private ConsistenciaMatricula buildConsistenciaMatricula(EstudianteDTO estudianteDTO) {
        ConsistenciaMatricula consistenciaMatricula = new ConsistenciaMatricula();
        consistenciaMatricula.setCodigo(Long.valueOf(String.valueOf(estudianteDTO.getPersonaCodigo()) + String.valueOf(DateUtil.getYear()) + String.valueOf(DateUtil.getMonthNumber())));
        consistenciaMatricula.setAnio(estudianteDTO.getAnio());
        consistenciaMatricula.setPersonaCod(estudianteDTO.getPersonaCodigo());
        consistenciaMatricula.setMesFormatoNumero(DateUtil.getMonthNumber());

        return consistenciaMatricula;
    }

    private GradoMetodologia buildGradoMetodologia(EstudianteDTO estudianteDTO) {
        GradoMetodologia gradoMetodologia = new GradoMetodologia();
        gradoMetodologia.setGradoCod(estudianteDTO.getGradoCod());
        gradoMetodologia.setMetodologiaCod(estudianteDTO.getMetodologiaCod());
        gradoMetodologia.setAnioMatricula(estudianteDTO.getAnio());
        gradoMetodologia.setMesMatricula(DateUtil.getMonthNumber());

        return gradoMetodologia;
    }

    private EstudianteGrado buildEstudianteGrado(EstudianteDTO estudianteDTO) {
        EstudianteGrado estudianteGrado = new EstudianteGrado();
        estudianteGrado.setPersonaCod(estudianteDTO.getPersonaCodigo());
        estudianteGrado.setGradoCod(estudianteDTO.getGradoCod());
        estudianteGrado.setAnioMatricula(estudianteDTO.getAnio());
        estudianteGrado.setMesMatricula(DateUtil.getMonthNumber());

        return estudianteGrado;
    }

    private EstudianteCaracter buildEstudianteCaracter(EstudianteDTO estudianteDTO) {
        EstudianteCaracter estudianteCaracter = new EstudianteCaracter();
        estudianteCaracter.setPersonaCod(estudianteDTO.getPersonaCodigo());
        estudianteCaracter.setCaracterCod(estudianteDTO.getCaracterCod());
        estudianteCaracter.setAnioMatricula(estudianteDTO.getAnio());
        estudianteCaracter.setMesMatricula(DateUtil.getMonthNumber());

        return estudianteCaracter;
    }

    private EstudianteSituacionAcademicaAnioAnterior buildEstudianteSitAcadAnioAnterior(EstudianteDTO estudianteDTO) {
        EstudianteSituacionAcademicaAnioAnterior estudianteSituacionAcademicaAnioAnterior = new EstudianteSituacionAcademicaAnioAnterior();
        estudianteSituacionAcademicaAnioAnterior.setPersonaCod(estudianteDTO.getPersonaCodigo());
        estudianteSituacionAcademicaAnioAnterior.setSituacionAcademicaAnioAnteriorCod(estudianteDTO.getSitAcadAnioAnteriorCod());
        estudianteSituacionAcademicaAnioAnterior.setAnioMatricula(estudianteDTO.getAnio());
        estudianteSituacionAcademicaAnioAnterior.setMesMatricula(DateUtil.getMonthNumber());

        return estudianteSituacionAcademicaAnioAnterior;
    }

    private EstudianteCondicionAcademicaAnioAnterior buildEstudianteCondAcadAnioAnterior(EstudianteDTO estudianteDTO) {
        EstudianteCondicionAcademicaAnioAnterior estudianteCondicionAcademicaAnioAnterior = new EstudianteCondicionAcademicaAnioAnterior();
        estudianteCondicionAcademicaAnioAnterior.setPersonaCod(estudianteDTO.getPersonaCodigo());
        estudianteCondicionAcademicaAnioAnterior.setCondicionAcademicaAnioAnteriorCod(estudianteDTO.getCondAcadAnioAnteriorCod());
        estudianteCondicionAcademicaAnioAnterior.setAnioMatricula(estudianteDTO.getAnio());
        estudianteCondicionAcademicaAnioAnterior.setMesMatricula(DateUtil.getMonthNumber());

        return estudianteCondicionAcademicaAnioAnterior;
    }

    private EstudianteEspecialidad buildEstudianteEspecialidad(EstudianteDTO estudianteDTO) {
        EstudianteEspecialidad estudianteEspecialidad = new EstudianteEspecialidad();
        estudianteEspecialidad.setPersonaCod(estudianteDTO.getPersonaCodigo());
        estudianteEspecialidad.setEspecialidadesCod(estudianteDTO.getEspecialidadCod());
        estudianteEspecialidad.setAnioMatricula(estudianteDTO.getAnio());
        estudianteEspecialidad.setMesMatricula(DateUtil.getMonthNumber());

        return estudianteEspecialidad;
    }

    private EstudianteTipoCapacidad buildEstudianteTipoCapacidad(EstudianteDTO estudianteDTO) {

        EstudianteTipoCapacidad estudianteTipoCapacidad = new EstudianteTipoCapacidad();
        estudianteTipoCapacidad.setPersonaCod(estudianteDTO.getPersonaCodigo());
        estudianteTipoCapacidad.setTipoCapacidad(estudianteDTO.getCapacidadCod());
        estudianteTipoCapacidad.setAnioMatricula(estudianteDTO.getAnio());
        estudianteTipoCapacidad.setMesMatricula(DateUtil.getMonthNumber());

        return estudianteTipoCapacidad;
    }

    private EstudianteTrastornoEspecifico buildEstudianteTrastornoEspecifico(EstudianteDTO estudianteDTO) {
        EstudianteTrastornoEspecifico estudianteTrastornoEspecifico = new EstudianteTrastornoEspecifico();
        estudianteTrastornoEspecifico.setPersonaCod(estudianteDTO.getPersonaCodigo());
        estudianteTrastornoEspecifico.setAnioMatricula(estudianteDTO.getAnio());
        estudianteTrastornoEspecifico.setMesMatricula(DateUtil.getMonthNumber());
        estudianteTrastornoEspecifico.setTrastornoEspecificoCod(estudianteDTO.getTrastornosEspecificosCod());

        return estudianteTrastornoEspecifico;
    }

    private EstudianteRespPenal buildEstudianteRespPenal(EstudianteDTO estudianteDTO) {
        EstudianteRespPenal estudianteRespPenal = new EstudianteRespPenal();
        estudianteRespPenal.setPersonaCod(estudianteDTO.getPersonaCodigo());
        estudianteRespPenal.setAnioMatricula(estudianteDTO.getAnio());
        estudianteRespPenal.setMesMatricula(DateUtil.getMonthNumber());
        estudianteRespPenal.setSistemaRespPenalCod(estudianteDTO.getSistemaRespPenalCod());

        return estudianteRespPenal;
    }

    private EstudianteFuenteRecurso buildEstudianteFuenteRecurso(EstudianteDTO estudianteDTO, Long fuenteRecursoCod) {
        EstudianteFuenteRecurso estudianteFuenteRecurso = new EstudianteFuenteRecurso();
        estudianteFuenteRecurso.setPersonaCod(estudianteDTO.getPersonaCodigo());
        estudianteFuenteRecurso.setFuenteRecursosCod(fuenteRecursoCod);

        return estudianteFuenteRecurso;
    }

    private EstudianteExpedicionDocumento buildEstudianteExpDocumento(EstudianteDTO estudianteDTO, Long municipioCod) {
        EstudianteExpedicionDocumento estudianteExpedicionDocumento = new EstudianteExpedicionDocumento();
        estudianteExpedicionDocumento.setPersonaCod(estudianteDTO.getPersonaCodigo());
        estudianteExpedicionDocumento.setMunicipioCod(municipioCod);

        return estudianteExpedicionDocumento;
    }

    private EstudianteResidencia buildEstudianteResidencia(EstudianteDTO estudianteDTO, Long municipioCod) {
        EstudianteResidencia estudianteResidencia = new EstudianteResidencia();
        estudianteResidencia.setPersonaCod(estudianteDTO.getPersonaCodigo());
        estudianteResidencia.setMunicipioCod(municipioCod);
        estudianteResidencia.setDireccion(estudianteDTO.getDireccionResidencia());

        return estudianteResidencia;
    }

    private EstudianteZona buildEstudianteZona(EstudianteDTO estudianteDTO) {
        EstudianteZona estudianteZona = new EstudianteZona();
        estudianteZona.setPersonaCod(estudianteDTO.getPersonaCodigo());
        estudianteZona.setZonaCod(estudianteDTO.getZonaCod());
        estudianteZona.setAnioMatricula(estudianteDTO.getAnio());
        estudianteZona.setMesMatricula(DateUtil.getMonthNumber());

        return estudianteZona;
    }

    private Municipio findMunicipio(Long municipioEstandarCod, Long departamentoEstandarCod) {
        Municipio municipioRes = municipioRepository.findByMunicipioEstandarCodAndDepartamentoEstandarCod(municipioEstandarCod, departamentoEstandarCod).orElse(new Municipio());
        return municipioRes;
    }

    private String findSector(String matriculaContratada) {
        String anexoType = jobParameters.getParameters().get("fileName").toString().substring(jobParameters.getParameters().get("fileName").toString().lastIndexOf("_") - 2, jobParameters.getParameters().get("fileName").toString().lastIndexOf("_"));

        if (anexoType.equalsIgnoreCase(Constant.ANEXO_5A) || anexoType.equalsIgnoreCase(Constant.ANEXO_5B) || anexoType.equalsIgnoreCase(Constant.ANEXO_5O)) {
            return Constant.SECTOR_PRIVADO;
        } else {
            if ("S".equalsIgnoreCase(matriculaContratada)) {
                return Constant.SECTOR_COBERTURA_CONTRATADA;
            } else {
                return Constant.SECTOR_OFICIAL;
            }
        }
    }


}
