package co.edu.medellin.sem.ocem.esencia.batch.estudiante.writer;

import co.edu.medellin.sem.ocem.esencia.repository.entity.Estudiante;
import co.edu.medellin.sem.ocem.esencia.repository.entity.EstudianteFuenteRecurso;
import co.edu.medellin.sem.ocem.esencia.repository.port.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.List;

/**
 * Esta clase es para hacer el paso de guardar la inforamcion en la base de datos
 *
 * @author israel.villegas@medellin.gov.co
 */

@Slf4j
public class EstudianteItemWriter implements ItemWriter<Estudiante> {


    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private EstudianteRepository estudianteRepository;

    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private ConsistenciaMatriculaRepository consistenciaMatriculaRepository;

    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private EstudianteVictimaConflictoRepository estudianteVictimaConflictoRepository;

    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private EstudianteApoyoAcademicoEspecialRepository estudianteApoyoAcademicoEspecialRepository;

    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private EstudianteZonaRepository estudianteZonaRepository;

    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private EstudianteResidenciaRepository estudianteResidenciaRepository;

    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private EstudianteExpedicionDocumentoRepository estudianteExpedicionDocumentoRepository;

    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private EstudianteFuenteRecursosRepository estudianteFuenteRecursosRepository;

    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private EstudianteRespPenalesRepository estudianteRespPenalesRepository;

    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private EstudianteTrastornoEspecificoRepository estudianteTrastornoEspecificoRepository;

    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private EstudianteTipoCapacidadRepository estudianteTipoCapacidadRepository;

    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private EstudianteEspecialidadRepository estudianteEspecialidadRepository;

    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private EstudianteSituacionAcademicaAnioAnteriorRepository estudianteSituacionAcademicaAnioAnteriorRepository;

    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private EstudianteCondicionAcademicaAnioAnteriorRepository estudianteCondicionAcademicaAnioAnteriorRepository;

    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private EstudianteCaracterRepository estudianteCaracterRepository;

    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private EstudianteGradoRepository estudianteGradoRepository;

    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private GradoMetodologiaRepository gradoMetodologiaRepository;

    /**
     * Metodo encargado de hacer el paso de guardar la informacion en la base de datos
     *
     * @param items
     * @throws Exception
     */
    @Override
    public void write(List<? extends Estudiante> items) {
        for (Estudiante item : items) {
            estudianteRepository.save(item);
            consistenciaMatriculaRepository.save(item.getConsistenciaMatricula());

            if (item.getEstudianteVictimaConflicto() != null)
                estudianteVictimaConflictoRepository.save(item.getEstudianteVictimaConflicto());

            if (item.getEstudianteApoyoAcademicoEspecial() != null)
                estudianteApoyoAcademicoEspecialRepository.save(item.getEstudianteApoyoAcademicoEspecial());

            if (item.getEstudianteZona() != null)
                estudianteZonaRepository.save(item.getEstudianteZona());

            if (item.getEstudianteResidencia() != null)
                estudianteResidenciaRepository.save(item.getEstudianteResidencia());

            if (item.getEstudianteExpedicionDocumento() != null && item.getEstudianteExpedicionDocumento().getMunicipioCod() != null)
                estudianteExpedicionDocumentoRepository.save(item.getEstudianteExpedicionDocumento());

            if (item.getEstudianteFuenteRecurso() != null)
                estudianteFuenteRecursosRepository.save(item.getEstudianteFuenteRecurso());

            if (item.getEstudianteRespPenal() != null)
                estudianteRespPenalesRepository.save(item.getEstudianteRespPenal());

            if (item.getEstudianteTrastornoEspecifico() != null)
                estudianteTrastornoEspecificoRepository.save(item.getEstudianteTrastornoEspecifico());

            if (item.getEstudianteTipoCapacidad() != null)
                estudianteTipoCapacidadRepository.save(item.getEstudianteTipoCapacidad());

            if (item.getEstudianteEspecialidad() != null)
                estudianteEspecialidadRepository.save(item.getEstudianteEspecialidad());

            if (item.getEstudianteSituacionAcademicaAnioAnterior() != null)
                estudianteSituacionAcademicaAnioAnteriorRepository.save(item.getEstudianteSituacionAcademicaAnioAnterior());

            if (item.getEstudianteCondicionAcademicaAnioAnterior() != null)
                estudianteCondicionAcademicaAnioAnteriorRepository.save(item.getEstudianteCondicionAcademicaAnioAnterior());

            if (item.getEstudianteCaracter() != null)
                estudianteCaracterRepository.save(item.getEstudianteCaracter());

            if (item.getEstudianteGrado() != null)
                estudianteGradoRepository.save(item.getEstudianteGrado());

            if (item.getGradoMetodologia() != null)
                gradoMetodologiaRepository.save(item.getGradoMetodologia());
        }
    }
}