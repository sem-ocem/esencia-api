package co.edu.medellin.sem.ocem.esencia.batch.ie;

import co.edu.medellin.sem.ocem.esencia.batch.ie.job.IEJobSkipPolicy;
import co.edu.medellin.sem.ocem.esencia.batch.ie.listener.IEJobListener;
import co.edu.medellin.sem.ocem.esencia.batch.ie.listener.IESkipListener;
import co.edu.medellin.sem.ocem.esencia.batch.ie.processor.IEItemProcessor;
import co.edu.medellin.sem.ocem.esencia.batch.ie.writer.IEItemWriter;
import co.edu.medellin.sem.ocem.esencia.model.IEDTO;
import co.edu.medellin.sem.ocem.esencia.repository.entity.IE;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.batch.item.excel.RowMapper;
import org.springframework.batch.item.excel.poi.PoiItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileUrlResource;

import java.net.MalformedURLException;


/**
 * Esta clase es para configurar el batch en el sistema
 *
 * @author israel.villegas@medellin.gov.co
 */
@EnableBatchProcessing
@Configuration
public class ConfigurationIEBatch {


    /**
     * Atributo que tiene el almacenado la ruta de la carpeta que contiene  los archivos planos para ser leidos y
     * almacenados en la base de datos
     */
    @Value("${FILE_LOAD}")
    private String fileLoad;

    /**
     * Atributo global que se encarga de contruir el job para hacer el proceso
     */
    @Autowired
    JobBuilderFactory jobBuilderFactory;


    /**
     * Es un Bean donde se ejecuta el segundo proceso del primer paso que tiene el job
     *
     * @return IEItemProcessor
     */
    @Bean
    public IEItemProcessor processIE() {
        return new IEItemProcessor();
    }

    /**
     * Es un Bean donde se ejecuta el tercer proceso del primer paso que tiene el job
     *
     * @return IEItemWriter
     */
    @Bean
    public IEItemWriter writeIE() {
        return new IEItemWriter();
    }


    /**
     * Este metodo es el encargado de hacer los llamados pora el proceso del batch
     *
     * @param ieJobListener Este parametros es un objeto que lo que hace es que se puede ejecutar antes y/o despues
     *                      de que el job funcione
     * @param stepIE1       Este atributo es un objeto donde le indica al yo los pasos que debe ejecutar
     * @return Job
     */
    @Bean("ieJob")
    public Job ieJob(IEJobListener ieJobListener, Step stepIE1) {
        return jobBuilderFactory.get("ieJob")
                .incrementer(new RunIdIncrementer())
                .listener(ieJobListener)
                .start(stepIE1)
                .build();
    }

    /**
     * Este metodo es el encargado de ejecutar los pasos que el job debe ejecutar que son leer, procesar y escribir;
     * pero el primer paso
     *
     * @param stepBuilderFactory este atributo es donde se ejecuta un step especifico
     * @param processIE          este parametro es un objeto donde es uno de los pasos que se debe ejcutar
     * @param writeIE            este parametro es un objeto donde es uno de los pasos que se debe ejcutar
     * @return Step
     * @throws Exception
     */
    @Bean
    public Step stepIE1(StepBuilderFactory stepBuilderFactory, IEItemProcessor processIE, IEItemWriter writeIE) throws Exception {
        return stepBuilderFactory.get("stepIE1")
                .<IEDTO, IE>chunk(1)
                .faultTolerant()
                .skipPolicy(new IEJobSkipPolicy())
                .reader(readIE())
                .processor(processIE)
                .writer(writeIE)
                .listener((StepExecutionListener) ieSkipListener())
                .build();
    }


    /**
     * Este metodo es el encargado de obtener la ruta de el archivo plano que se va a procesar
     *
     * @param fileName Este parametro es el que tiene el nombre del archivo plano que se debe ejecutar
     * @return FileSystemResource
     */

    @Bean
    @StepScope
    FileUrlResource fileSystemRescourcesIE(@Value("#{jobParameters['fileName']}") final String fileName) throws MalformedURLException {
        return new FileUrlResource(fileLoad + fileName);
    }


    /**
     * Este metodo es el encargado de leer la informacion del excel
     *
     * @return ItemReader<IEDTO>
     * @throws Exception
     * @throws UnexpectedInputException
     * @throws ParseException
     * @throws NonTransientResourceException
     */
    @Bean
    public ItemReader<IEDTO> readIE() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
        PoiItemReader<IEDTO> reader = new PoiItemReader<>();
        reader.setResource(fileSystemRescourcesIE(null));
        reader.setLinesToSkip(1);
        reader.setRowMapper(excelRowMapper());

        return reader;
    }

    /**
     * Este metodo se encarga de ir la clase IEExcelRowMapper y ejecutar el metodo mapRow
     *
     * @return
     */
    private RowMapper<IEDTO> excelRowMapper() {
        return new IEExcelRowMapper();
    }


    /**
     * Bean que se encarga de hacer el trabajo de saltar los registros que tienen errores
     * @return JobSkipPolicy
     */
    /*
    @Bean
    public IEJobSkipPolicy ieSkinPolicy(){
        return new IEJobSkipPolicy();
    }
*/
    /**
     * Bean que se encarga en capturar los registros que fueron evitados en algunos de los pasos (reader, process or
     * writter)
     * @return IESkipListener
     */
    @Bean
    public IESkipListener ieSkipListener (){
        return new IESkipListener();
    }

}
