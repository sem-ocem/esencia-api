package co.edu.medellin.sem.ocem.esencia.batch.estudiante;

import co.edu.medellin.sem.ocem.esencia.batch.estudiante.job.JobSkipPolicy;
import co.edu.medellin.sem.ocem.esencia.batch.estudiante.listener.*;
import co.edu.medellin.sem.ocem.esencia.batch.estudiante.processor.EstudianteItemProcessor;
import co.edu.medellin.sem.ocem.esencia.batch.estudiante.writer.EstudianteItemWriter;
import co.edu.medellin.sem.ocem.esencia.model.EstudianteDTO;
import co.edu.medellin.sem.ocem.esencia.repository.entity.Estudiante;
import co.edu.medellin.sem.ocem.esencia.util.Constant;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;


/**
 * Esta clase es para configurar el batch en el sistema
 *
 * @author israel.villegas@medellin.gov.co
 */
@EnableBatchProcessing
@Configuration
public class ConfigurationBatch {

    /**
     * Atributo que tiene el almacenado la ruta de la carpeta que contiene  los archivos planos para ser leidos y
     * almacenados en la base de datos
     */
    @Value("${FILE_LOAD}")
    private String fileLoad;

    /**
     * Atributo global que se encarga de contruir el job para hacer el proceso
     */
    @Autowired
    JobBuilderFactory jobBuilderFactory;

    /**
     * Es un Bean donde se ejecuta el segundo proceso del primer paso que tiene el job
     *
     * @return EstudianteItemProcessor
     */
    @Bean
    public EstudianteItemProcessor process() {
        return new EstudianteItemProcessor();
    }

    /**
     * Es un Bean donde se ejecuta el tercer proceso del primer paso que tiene el job
     *
     * @return AnexoItemEscritura
     */
    @Bean
    public EstudianteItemWriter Write() {
        return new EstudianteItemWriter();
    }


    /**
     * Este metodo es el encargado de hacer los llamados pora el proceso del batch
     * @param estudianteJobListener Este parametros es un objeto que lo que hace es que se puede ejecutar antes y/o despues
     *  de que el job funcione
     * @param step1 Este atributo es un objeto donde le indica al yo los pasos que debe ejecutar
     * @return Job
     */
    @Bean("estudianteJob")
    public Job estudianteJob(EstudianteJobListener estudianteJobListener, Step step1) {
        return jobBuilderFactory.get("estudianteJob")
                .incrementer(new RunIdIncrementer())
                .listener(estudianteJobListener)
                .start(step1)
                .build();
    }

    /**
     * Este metodo es el encargado de ejecutar los pasos que el job debe ejecutar que son leer, procesar y escribir;
     * pero el primer paso
     * @param stepBuilderFactory este atributo es donde se ejecuta un step especifico
     * @param process este parametro es un objeto donde es uno de los pasos que se debe ejcutar
     * @param writer este parametro es un objeto donde es uno de los pasos que se debe ejcutar
     * @return Step
     * @throws Exception
     */
    @Bean
    public Step step1(StepBuilderFactory stepBuilderFactory, EstudianteItemProcessor process, EstudianteItemWriter writer) throws Exception {
        return stepBuilderFactory.get("step1")
                .<EstudianteDTO, Estudiante>chunk(1)
                .reader(read())
                .faultTolerant()
                .skipPolicy(new JobSkipPolicy())
                .processor(process)
                .writer(writer)
                .listener((StepExecutionListener) estudianteSkipListener())
                .build();
    }

    /**
     * Metodo que se encarga de leer el archivo plano y mappearlo a el objeto que se necesita
     *
     * @return FlatFileItemReader<EstudianteDTO>
     * @throws Exception
     */
    @Bean
    @StepScope
    public FlatFileItemReader<EstudianteDTO> read() throws Exception {

        FlatFileItemReader<EstudianteDTO> leer = new FlatFileItemReader<>();
        leer.setResource(fileSystemRescources(null));
        leer.setLinesToSkip(1);
        DelimitedLineTokenizer lineTokeniser = new DelimitedLineTokenizer(";");
        lineTokeniser.setIncludedFields(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22,
                23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48,
                49, 50, 51, 52, 53, 54, 55, 56, 57, 58);
        leer.setLineMapper(new DefaultLineMapper<EstudianteDTO>() {{
            setLineTokenizer(lineTokeniser);
            lineTokeniser.setNames(Constant.PROPIEDADES_ANEXO);
            setFieldSetMapper(new BeanWrapperFieldSetMapper<EstudianteDTO>() {{
                setTargetType(EstudianteDTO.class);
            }});
        }});
        return leer;
    }

    /**
     * Este metodo es el encargado de obtener la ruta de el archivo plano que se va a procesar
     * @param fileName Este parametro es el que tiene el nombre del archivo plano que se debe ejecutar
     * @return FileSystemResource
     */
    @Bean
    @StepScope
    FileSystemResource fileSystemRescources(@Value("#{jobParameters['fileName']}") final String fileName) {
        return new FileSystemResource(fileLoad + fileName);
    }

    /**
     * Bean que se encarga de hacer el trabajo de saltar los registros que tienen errores
     * @return JobSkipPolicy
     */
    /*
    @Bean
    public JobSkipPolicy skinPolicy(){
        return new JobSkipPolicy();
    }
*/
    /**
     * Bean que se encarga en capturar los registros que fueron evitados en algunos de los pasos (reader, process or
     * writter)
     * @return EstudianteSkipListener
     */
    @Bean
    public EstudianteSkipListener estudianteSkipListener (){
        return new EstudianteSkipListener();
    }

}
