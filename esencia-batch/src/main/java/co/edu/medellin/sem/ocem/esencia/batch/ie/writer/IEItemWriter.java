package co.edu.medellin.sem.ocem.esencia.batch.ie.writer;

import co.edu.medellin.sem.ocem.esencia.repository.entity.Estudiante;
import co.edu.medellin.sem.ocem.esencia.repository.entity.IE;
import co.edu.medellin.sem.ocem.esencia.repository.port.ConsistenciaIERepository;
import co.edu.medellin.sem.ocem.esencia.repository.port.ConsistenciaMatriculaRepository;
import co.edu.medellin.sem.ocem.esencia.repository.port.IERepository;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Esta clase es para hacer el paso de guardar la inforamcion en la base de datos
 *
 * @author israel.villegas@medellin.gov.co
 */
public class IEItemWriter implements ItemWriter<IE> {


    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private IERepository ieRepository;

    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private ConsistenciaIERepository consistenciaIERepository;


    /**
     * Metodo encargado de hacer el paso de guardar la informacion en la base de datos
     *
     * @param items
     * @throws Exception
     */
    @Override
    public void write(List<? extends IE> items) throws Exception {
        for (IE item : items) {
            ieRepository.save(item);
            consistenciaIERepository.save(item.getConsistenciaIE());
        }
    }

}
