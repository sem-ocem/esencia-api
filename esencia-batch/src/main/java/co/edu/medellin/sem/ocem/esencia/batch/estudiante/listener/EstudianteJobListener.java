package co.edu.medellin.sem.ocem.esencia.batch.estudiante.listener;


import co.edu.medellin.sem.ocem.esencia.repository.entity.ConsistenciaIE;
import co.edu.medellin.sem.ocem.esencia.repository.port.ConsistenciaIERepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.batch.operations.JobOperator;
import javax.batch.runtime.BatchRuntime;
import java.io.File;
import java.util.List;
import java.util.Set;

/**
 * Esta clase extiende de la clase JobExecutionListenerSupport y es para informar en que estado esta el job
 * @author israel.villegas@medellin.gov.co
 */

@Slf4j
@Component
public class EstudianteJobListener extends JobExecutionListenerSupport {

    /**
     * Atributo que tiene el almacenado la ruta de la carpeta que contiene  los archivos planos para ser leidos y
     * almacenados en la base de datos
     */
    @Value("${FILE_LOAD}")
    private String fileLoad;

    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private ConsistenciaIERepository consistenciaIERepository;


    @Override
    public void beforeJob(JobExecution jobExecution){
  /*      File file = new File(fileSimcie+"/"+jobExecution.getJobParameters().getParameters().get("fileName"));
        String nameReal = file.getName();
        int pos = nameReal.lastIndexOf(".");
        nameReal = nameReal.substring(0, pos);
        List<ConsistenciaIE> consistenciaIEs = consistenciaIERepository.lastMoth(Integer.parseInt(nameReal.substring(nameReal.length()-1)));
        if(consistenciaIEs == null || consistenciaIEs.isEmpty()){
            jobExecution.stop();
        }
   */
    }

    /**
     * Metodo que se encarga de mostrara en que estado quedo el job ejecutado
     * @param jobExecution es el objeto que muestra las caracteristicas del job ejecutado
     */
    @Override
    public void afterJob(JobExecution jobExecution){
        if(jobExecution.getStatus() == BatchStatus.COMPLETED){
        }

        File file = new File(fileLoad+"/"+jobExecution.getJobParameters().getParameters().get("fileName"));
        String nameReal = file.getName();
        file.renameTo(new File(file.getParent(), nameReal+"_"+jobExecution.getStatus()));

        log.info("CANTIDAD DE REGISTROS "+ jobExecution.getStepExecutions().iterator().next().getReadCount());
        log.info("CANTIDAD DE REGISTROS PROCESADOS "+ jobExecution.getStepExecutions().iterator().next().getWriteCount());
        log.info("CANTIDAD DE REGISTROS CON ERRORES "+ jobExecution.getStepExecutions().iterator().next().getProcessSkipCount());

        int ghostRecord = jobExecution.getStepExecutions().iterator().next().getReadCount() - (jobExecution.getStepExecutions().iterator().next().getWriteCount() + jobExecution.getStepExecutions().iterator().next().getProcessSkipCount());
        log.info("CANTIDAD DE REGISTROS FANTASMAS "+ ghostRecord);
    }

}
