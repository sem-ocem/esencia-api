package co.edu.medellin.sem.ocem.esencia.batch.ie.listener;


import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.batch.operations.JobOperator;
import javax.batch.runtime.BatchRuntime;
import java.io.File;

/**
 * Esta clase extiende de la clase JobExecutionListenerSupport y es para informar en que estado esta el job
 *
 * @author israel.villegas@medellin.gov.co
 */

@Component
public class IEJobListener extends JobExecutionListenerSupport {

    /**
     * Atributo que tiene el almacenado la ruta de la carpeta que contiene  los archivos planos para ser leidos y
     * almacenados en la base de datos
     */
    @Value("${FILE_LOAD}")
    private String fileLoad;

    /**
     * Metodo que se encarga de mostrara en que estado quedo el job ejecutado
     *
     * @param jobExecution es el objeto que muestra las caracteristicas del job ejecutado
     */
    @Override
    public void afterJob(JobExecution jobExecution) {
        if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
        }

        File file = new File(fileLoad + "/" + jobExecution.getJobParameters().getParameters().get("fileName"));
        String nameReal = file.getName();
        file.renameTo(new File(file.getParent(), nameReal + "_" + jobExecution.getStatus()));
    }

}
