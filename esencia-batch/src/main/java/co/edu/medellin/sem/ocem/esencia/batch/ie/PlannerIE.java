package co.edu.medellin.sem.ocem.esencia.batch.ie;

import co.edu.medellin.sem.ocem.esencia.util.enums.FileNameLoad;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * Esta clase es donde esta establecido el tiempo en que se debe ejecutar el job
 *
 * @author israel.villegas@medellin.gov.co
 */
@Component
@EnableScheduling
public class PlannerIE {

    @Value("${FILE_LOAD}")
    private String fileLoad;

    /**
     * Variable global que se encarga de inyectar el JobLauncher para despertar el job para ejecutar
     */
    @Autowired
    private JobLauncher jobLauncherIE;

    /**
     * Variable global que se encarga de inyectar el Job, que es que va hacer el trabajo de leer el archivo plano para
     * guardarlo en la base de datos
     */
    @Autowired
    private Job ieJob;


    /**
     * Metodo que se ejecuta de acuerdo al periodo establecido para ejecutar el batch
     */
    //@Scheduled(cron = "5 5 5 5 5 5")
    @Scheduled(cron = "0 * * * * *")
    protected void start() {
        String[] nombresArchivos = getFileName();
        for (String nombreArchivo : nombresArchivos) {
            Map<String, JobParameter> maps = new HashMap<>();
            maps.put("timestamp", new JobParameter(Long.valueOf((new Date()).getTime())));
            maps.put("fileName", new JobParameter(nombreArchivo));
            JobParameters jobParameters = new JobParameters(maps);
            try {
                this.jobLauncherIE.run(this.ieJob, jobParameters);
            } catch (JobExecutionAlreadyRunningException | org.springframework.batch.core.repository.JobRestartException | org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException | org.springframework.batch.core.JobParametersInvalidException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Metodo que se encarga de obtener la lista de archvios que se necesita procesar
     *
     * @return String[]
     */
    private String[] getFileName() {
        File carpeta = new File(fileLoad);
        ArrayList<String> listaArchivos = new ArrayList<String>();
        for (File archivo : carpeta.listFiles())
            if (FileNameLoad.DUE.name().toLowerCase().concat("_").concat(FileNameLoad.DUE.getNameComplementFile()).equals(archivo.getName()))
                listaArchivos.add(archivo.getName());
        return (listaArchivos.toArray(new String[0]));
    }
}
