package co.edu.medellin.sem.ocem.esencia.batch.ie.listener;

import co.edu.medellin.sem.ocem.esencia.model.IEDTO;
import co.edu.medellin.sem.ocem.esencia.repository.entity.ErrorRegistro;
import co.edu.medellin.sem.ocem.esencia.repository.entity.IE;
import co.edu.medellin.sem.ocem.esencia.repository.port.ErrorRegistroRepository;
import co.edu.medellin.sem.ocem.esencia.util.Constant;
import co.edu.medellin.sem.ocem.esencia.util.exception.CustomException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.*;
import org.springframework.batch.item.excel.ExcelFileParseException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Esta clase es la encargada de escuchar cuando un registro fue evitado y obtener el objeto y su excepcion
 */
@Slf4j
public class IESkipListener implements SkipListener<IEDTO, IE>, StepExecutionListener {


    /**
     * Atributo global que es inyectado para tener acceso a la capa repositorio
     */
    @Autowired
    private ErrorRegistroRepository errorRegistroRepository;

    JobParameters jobParameters;

    @Override
    public void beforeStep(final StepExecution stepExecution) {
        jobParameters = stepExecution.getJobParameters();
    }


    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        return null;
    }


    @Override
    public void onSkipInRead(Throwable t) {
        ErrorRegistro errorRegistro = new ErrorRegistro();
        if (t instanceof ExcelFileParseException){
            ExcelFileParseException excelFileParseException = (ExcelFileParseException) t;
            String datasTmp[] = excelFileParseException.getRow();
            errorRegistro.setRegistroCod(Long.parseLong(datasTmp[3]));
            CustomException customException = (CustomException) t.getCause();
            errorRegistro.setDescripcion(customException.getMessage());
        }else {
            errorRegistro.setRegistroCod(null);
        }
        errorRegistro.setNombreArchivo(jobParameters.getParameters().get("fileName").toString());
        errorRegistro.setTrazaError(t.getMessage());
        if (t.getMessage().equals(Constant.NO_SUPPORT_THESE_TYPE))
            errorRegistro.setDescripcion("Buscar en el archivo xlsx el dato erroneo #N/A");
        errorRegistroRepository.save(errorRegistro);
    }

    @Override
    public void onSkipInWrite(IE item, Throwable t) {
        ErrorRegistro errorRegistro = new ErrorRegistro();
        errorRegistro.setNombreArchivo(jobParameters.getParameters().get("fileName").toString());
        errorRegistro.setRegistroCod(item.getDaneAnterior());
        errorRegistro.setTrazaError(t.getMessage());
        errorRegistroRepository.save(errorRegistro);
    }

    @Override
    public void onSkipInProcess(IEDTO item, Throwable t) {
        ErrorRegistro errorRegistro = new ErrorRegistro();
        errorRegistro.setNombreArchivo(jobParameters.getParameters().get("fileName").toString());
        errorRegistro.setRegistroCod(item.getDaneAnterior());
        errorRegistro.setTrazaError(t.toString());
        errorRegistro.setDescripcion(t.getMessage());
        if(t instanceof CustomException){
            errorRegistro.setNombreVariableArchivo(((CustomException) t).getVariableFile());
        }
        errorRegistroRepository.save(errorRegistro);
    }
}