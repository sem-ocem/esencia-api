# Read Me First
The following was discovered as part of building this project:

* The original package name 'co.edu.medellin.sem.ocem.esencia-api' is invalid and this project uses 'co.edu.medellin.sem.ocem.esenciaapi' instead.

# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Gradle documentation](https://docs.gradle.org)
* [Spring Boot Gradle Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.4.4/gradle-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.4.4/gradle-plugin/reference/html/#build-image)
* [Spring Batch](https://docs.spring.io/spring-boot/docs/2.4.4/reference/htmlsingle/#howto-batch-applications)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/2.4.4/reference/htmlsingle/#boot-features-jpa-and-spring-data)

### Guides
The following guides illustrate how to use some features concretely:

* [Creating a Batch Service](https://spring.io/guides/gs/batch-processing/)
* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)

### Additional Links
These additional references should also help you:

* [Gradle Build Scans – insights for your project's build](https://scans.gradle.com#gradle)




### Variables de entorno
* BD_PASSWORD=<PASSWORD DE LA BASE DE DATOS>;
* BD_URL=jdbc:postgresql://localhost:<PUERTO>/<NOMBRE DE LA BASE DE DATOS>?currentSchema=<NOMBRE DEL ESQUEMA>;
* BD_USER_NAME=postgres;
* SECURITY_TYPE=BASIC;
* FILE_LOAD=<RUTA DONDE SE GUARDA LOS ARCHIVOS QUE SE VAN A CARVAR EN LA BASE DE DATOS DE LA OCEM>


### REPOSITORIO
* Este repositorio se clonara en esta direccion "https://semRep@bitbucket.org/sem-ocem/esencia-api.git", para clonarlo
se ejecuta este comando "git clone https://semRep@bitbucket.org/sem-ocem/esencia-api.git"

### LINUX COMANDO PARA CONVERTIR EL ARCHIVO TXT QUE ESTA EN iso-8859-1 CONVERTIRLO EN UTF-8
* Consultar en que formato esta
    "file -bi <nombre_archivo>"
** Hacer las conversion 
    "iconv -f <formato_original> -t utf8 <nombre_archivo> > <nombre_archivo_nuevo>"
    ejemplo "iconv -f iso-8859-1 -t utf8 anexo_5a_20215_.txt > anexo_5a_20215.txt     
    
# SQLs
* Existe una carpeta dentro del proyecto que se llama sql donde esta almacenado los DML y DDL

### PROCESOS DE LOS BATCH
Se debe ejecutar primero el batch de las IEs y despues el batch de los estudiantes

#### NOTAS
Cuando se crea la base de datos en la tabla eo_tbl_municipio se debe poner como PK los atributos codigo y departamento_codigo

